export MADGRAPH5=/users/whopkins/sigclustering/sigclustering/MG5_aMC_v2_9_5
export Delphes=$MADGRAPH5/Delphes/
export Delphes_external=$Delphes/external/

export PATH=$Delphes:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$Delphes
export ROOT_INCLUDE_PATH=$Delphes/classes/

#ExRootAnalysis
export ExRoot=$Delphes_external/ExRootAnalysis
export PATH=$ExRoot:$PATH
export LD_LIBRARY_PATH=$ExRoot:$LD_LIBRARY_PATH
export ROOT_INCLUDE_PATH=$ExRoot

