/*
  Simple macro showing how to access branches from the delphes output root file,
  loop over events, and plot simple quantities such as the jet pt and the di-electron invariant
  mass.

  root -l snowmass_pMSSM.C'("delphes_output.root")'
*/

#ifdef __CLING__
R__LOAD_LIBRARY(libDelphes)
#include "classes/DelphesClasses.h"
#include "external/ExRootAnalysis/ExRootTreeReader.h"
#endif

//------------------------------------------------------------------------------
/**
 * This function finds the object that is closest to the MET given the 
 MET, phi of the MET, and the max element. Ordering of the object matters since
 the max element arugment will determine when you stop. For example, PT is a sensible order
 of the inputs.
*/
float findMETMinDPhi(const std::vector<TLorentzVector> &objects,
                     const float &met_phi, int &minIndex, int maxEl=-1){
	float minDPhi = 99;
	float dPhi = 99;
	int index = 0;
	for (const TLorentzVector& object : objects){
		if (index >= maxEl && maxEl > 0) 
			break;
		if (object.Pt() < 0)
			continue;
		dPhi = fabs(TVector2::Phi_mpi_pi(object.Phi()-met_phi));

		if (dPhi < minDPhi) {
			minIndex = index;
			minDPhi = dPhi;
		}
		index++;
	}
	return minDPhi;
}
/**
 * Calculate the transverse mass given a set of objects.
 */
float calcClosestMT(const std::vector<TLorentzVector> &objects,
                    const float &met, const float &met_phi)
{
	int minIndex = -1;
	float minDPhi = findMETMinDPhi(objects, met_phi, minIndex);
	if (minIndex < 0)
		return -99;

	TLorentzVector minObject=objects[minIndex];
	float mt = -99;
	if (fabs(minDPhi) < TMath::Pi())
		mt = TMath::Sqrt(2*minObject.Pt()*met*(1-TMath::Cos(minDPhi)));
	return mt;
}


void snowmass_pMSSM(const char *inputFName, const char *outputFName, int maxEvents=-1,
           // These should probably be loaded from some sort of config file...
           int maxNJets=10, float minJetPT=20, float maxJetEta=2.8, int minNJets=0,
           int minNBJets=0, float minMET=0)
{
	gSystem->Load("libDelphes");

	TFile * outputF = TFile::Open(outputFName, "RECREATE");
	TTree * tree = new TTree("Nominal", "Flat tree based on Delphes input.");

	// Jet four vector information
	int nJets;
	int nBJets;

	Int_t nGoodJets = 0;
	Int_t nGoodBJets = 0;
	Int_t nFJets;
	std::vector<Float_t> jetPt(maxNJets);
	std::vector<Float_t> jetEta(maxNJets);
	std::vector<Float_t> jetPhi(maxNJets);
	std::vector<Float_t> jetM(maxNJets);

	std::vector<Float_t> bJetPt(maxNJets);
	std::vector<Float_t> bJetEta(maxNJets);
	std::vector<Float_t> bJetPhi(maxNJets);
	std::vector<Float_t> bJetM(maxNJets);

	// std::vector<Float_t> fJetPt(maxNJets);
	// std::vector<Float_t> fJetEta(maxNJets);
	// std::vector<Float_t> fJetPhi(maxNJets);
	// std::vector<Float_t> fJetM(maxNJets);

	// Initialize our output variables;
	Float_t initVal = -9999;
	std::fill(jetPt.begin(), jetPt.end(), initVal);
	std::fill(jetEta.begin(), jetEta.end(), initVal);
	std::fill(jetPhi.begin(), jetPhi.end(), initVal);
	std::fill(jetM.begin(), jetM.end(), initVal);

	std::fill(bJetPt.begin(), bJetPt.end(), initVal);
	std::fill(bJetEta.begin(), bJetEta.end(), initVal);
	std::fill(bJetPhi.begin(), bJetPhi.end(), initVal);
	std::fill(bJetM.begin(), bJetM.end(), initVal);
			
	// std::fill(fJetPt.begin(), fJetPt.end(), initVal);
	// std::fill(fJetEta.begin(), fJetEta.end(), initVal);
	// std::fill(fJetPhi.begin(), fJetPhi.end(), initVal);
	// std::fill(fJetM.begin(), fJetM.end(), initVal);

	tree->Branch("nj_good",&nGoodJets,"nj_good/I");
	tree->Branch("num_bjets",&nGoodBJets,"num_bjets/I");
	std::string branchName;
	Float_t testPt = 0;
	for (uint jetI=0; jetI < maxNJets; ++jetI) {
		branchName = "pT_"+std::to_string(jetI+1)+"jet";
		tree->Branch(branchName.c_str(), &jetPt.at(jetI), (branchName+"/F").c_str());
		//tree->Branch("pT_1jet", &testPt, "pT_1jet/F");

		branchName = "eta_"+std::to_string(jetI+1)+"jet";
		tree->Branch(branchName.c_str(), &jetEta.at(jetI), (branchName+"/F").c_str());
		branchName = "phi_"+std::to_string(jetI+1)+"jet";
		tree->Branch(branchName.c_str(), &jetPhi.at(jetI), (branchName+"/F").c_str());
		branchName = "m_"+std::to_string(jetI+1)+"jet";
		tree->Branch(branchName.c_str(), &jetM.at(jetI), (branchName+"/F").c_str());
	}
	for (uint jetI=0; jetI < maxNJets; ++jetI) {
		branchName = "pT_"+std::to_string(jetI+1)+"bjet";
		tree->Branch(branchName.c_str(), &bJetPt.at(jetI), (branchName+"/F").c_str());
		branchName = "eta_"+std::to_string(jetI+1)+"bjet";
		tree->Branch(branchName.c_str(), &bJetEta.at(jetI), (branchName+"/F").c_str());
		branchName = "phi_"+std::to_string(jetI+1)+"bjet";
		tree->Branch(branchName.c_str(), &bJetPhi.at(jetI), (branchName+"/F").c_str());
		branchName = "m_"+std::to_string(jetI+1)+"bjet";
		tree->Branch(branchName.c_str(), &bJetM.at(jetI), (branchName+"/F").c_str());
	}

	// for (uint jetI=0; jetI < maxNJets; ++jetI) {
	// 	branchName = "pT_"+std::to_string(jetI+1)+"fjet";
	// 	tree->Branch(branchName.c_str(), &fJetPt.at(jetI), (branchName+"/F").c_str());
	// 	branchName = "eta_"+std::to_string(jetI+1)+"fjet";
	// 	tree->Branch(branchName.c_str(), &fJetEta.at(jetI), (branchName+"/F").c_str());
	// 	branchName = "phi_"+std::to_string(jetI+1)+"fjet";
	// 	tree->Branch(branchName.c_str(), &fJetPhi.at(jetI), (branchName+"/F").c_str());
	// 	branchName = "m_"+std::to_string(jetI+1)+"fjet";
	// 	tree->Branch(branchName.c_str(), &fJetM.at(jetI), (branchName+"/F").c_str());
	// }

	Float_t met;
	Float_t met_phi;
	Float_t ht;
	tree->Branch("MET",&met,"MET/f");
	tree->Branch("MET_phi",&met_phi,"MET_phi/f");
	tree->Branch("HT",&ht,"HT/f");

	Float_t minDPhiMetJet1;
	Float_t minDPhiMetJet2;
	Float_t minDPhiMetJet3;
	Float_t minDPhiMetJet4;
	tree->Branch("dphimin1",&minDPhiMetJet1,"dphimin1/f");
	tree->Branch("dphimin2",&minDPhiMetJet2,"dphimin2/f");
	tree->Branch("dphimin3",&minDPhiMetJet3,"dphimin3/f");
	tree->Branch("dphimin4",&minDPhiMetJet4,"dphimin4/f");

	Float_t mTBMin;
	tree->Branch("MTbmin",&mTBMin,"MTbmin/f");

	// Create chain of root trees
	TChain chain("Delphes");
	chain.Add(inputFName);
  
	// Create object of class ExRootTreeReader
	ExRootTreeReader *treeReader = new ExRootTreeReader(&chain);
	Long64_t numberOfEntries = treeReader->GetEntries();
	if (maxEvents > 0) {
		numberOfEntries = maxEvents;
	}

	// Get pointers to branches used in this analysis
	TClonesArray *branchJet = treeReader->UseBranch("KTjet");
	TClonesArray *branchElectron = treeReader->UseBranch("Electron");
	TClonesArray *branchMuon = treeReader->UseBranch("Muon");
	TClonesArray *branchEvent = treeReader->UseBranch("Event");
	TClonesArray *branchMissingET = treeReader->UseBranch("MissingET");
	//TClonesArray *branchScalarHT = treeReader->UseBranch("ScalarHT");
	//TClonesArray *branchFatJet = treeReader->UseBranch("FatJet");

	MissingET *missingET;
	//ScalarHT *scalarHT;

	std::vector<TLorentzVector> goodJets;
	std::vector<TLorentzVector> goodBJets;
	//std::vector<TLorentzVector> goodFJets;
	
	int minIndex = -1;

	// Loop over all events
	for(Int_t entry = 0; entry < numberOfEntries; ++entry)
		{	
			// Load selected branches with data from specified event
			treeReader->ReadEntry(entry);
    
			// Initialize our output variables;
			std::fill(jetPt.begin(), jetPt.end(), initVal);
			std::fill(jetEta.begin(), jetEta.end(), initVal);
			std::fill(jetPhi.begin(), jetPhi.end(), initVal);
			std::fill(jetM.begin(), jetM.end(), initVal);

			std::fill(bJetPt.begin(), bJetPt.end(), initVal);
			std::fill(bJetEta.begin(), bJetEta.end(), initVal);
			std::fill(bJetPhi.begin(), bJetPhi.end(), initVal);
			std::fill(bJetM.begin(), bJetM.end(), initVal);
			
			// std::fill(fJetPt.begin(), fJetPt.end(), initVal);
			// std::fill(fJetEta.begin(), fJetEta.end(), initVal);
			// std::fill(fJetPhi.begin(), fJetPhi.end(), initVal);
			// std::fill(fJetM.begin(), fJetM.end(), initVal);

			//HepMCEvent *event = (HepMCEvent*) branchEvent -> At(0);
			//LHEFEvent *event = (LHEFEvent*) branchEvent -> At(0);
			//Float_t weight = event->Weight;

			ht = 0;
			mTBMin = 99999;
			nGoodJets = 0;
			nGoodBJets = 0;
			nJets = branchJet->GetEntries();
			if (nJets == 0)
				continue;
			
			for (uint jetI=0; jetI<nJets; ++jetI) {
				Jet *jet = (Jet*) branchJet->At(jetI);
				if (jet->PT > minJetPT && fabs(jet->Eta) < maxJetEta) {
					TLorentzVector tempTLV = TLorentzVector();
					tempTLV.SetPtEtaPhiM(jet->PT, jet->Eta, jet->Phi, jet->Mass);
					goodJets.push_back(tempTLV);
					
					ht+=tempTLV.Pt();
					nGoodJets++;
					if (jet->BTag){
						nGoodBJets++;
						goodBJets.push_back(tempTLV);
					}
				}
			}

			if (nGoodJets < minNJets)
				continue;

			if (nGoodBJets < minNBJets)
				continue;

			// std::sort(goodJets.begin(), goodJets.end(), 
			//           [](const TLorentzVector & a, const TLorentzVector & b) -> bool
			//           { 
			// 	          return a.Pt() > b.Pt(); 
			//           });

			// std::sort(goodBJets.begin(), goodBJets.end(), 
			//           [](const TLorentzVector & a, const TLorentzVector & b) -> bool
			//           { 
			// 	          return a.Pt() > b.Pt(); 
			//           });

			nJets = (nGoodJets < maxNJets) ? nGoodJets : maxNJets;
			for (uint jetI=0; jetI<nJets; ++jetI) {
				
				jetPt[jetI]=goodJets.at(jetI).Pt();
				jetEta[jetI]=goodJets.at(jetI).Eta();
				jetPhi[jetI]=goodJets.at(jetI).Phi();
				jetM[jetI]=goodJets.at(jetI).M();
			}

			nBJets = (nGoodBJets < maxNJets) ? nGoodBJets : maxNJets;
			for (uint jetI=0; jetI<nBJets; ++jetI) {
				bJetPt[jetI]=goodBJets.at(jetI).Pt();
				bJetEta[jetI]=goodBJets.at(jetI).Eta();
				bJetPhi[jetI]=goodBJets.at(jetI).Phi();
				bJetM[jetI]=goodBJets.at(jetI).M();
			}

			// nFJets = branchJet->GetEntries();
			
			// for (uint jetI=0; jetI<nFJets; ++jetI) {
			// 	Jet *jet = (Jet*) branchJet->At(jetI);
			// 	if (jet->PT > minJetPT && fabs(jet->Eta) < maxJetEta) {
			// 		TLorentzVector tempTLV = TLorentzVector();
			// 		tempTLV.SetPtEtaPhiM(jet->PT, jet->Eta, jet->Phi, jet->Mass);
			// 		goodFJets.push_back(tempTLV);
			// 	}
			// }

			// nFJets = (nFJets < maxNJets) ? nFJets : maxNJets;
			// for (uint jetI=0; jetI<nFJets; ++jetI) {
			// 	fJetPt[jetI]=goodFJets.at(jetI).Pt();
			// 	fJetEta[jetI]=goodFJets.at(jetI).Eta();
			// 	fJetPhi[jetI]=goodFJets.at(jetI).Phi();
			// 	fJetM[jetI]=goodFJets.at(jetI).M();
			// }
			// Analyse missing ET
			if(branchMissingET->GetEntriesFast() > 0)
				{
					missingET = (MissingET*) branchMissingET->At(0);
					met = missingET->MET;
					met_phi = missingET->Phi;

					if (met < minMET) 
						continue;
				}
			minIndex = -1;
			minDPhiMetJet1 = findMETMinDPhi(goodJets, met_phi, minIndex, 1);
			minDPhiMetJet2 = findMETMinDPhi(goodJets, met_phi, minIndex, 2);
			minDPhiMetJet3 = findMETMinDPhi(goodJets, met_phi, minIndex, 3);
			minDPhiMetJet4 = findMETMinDPhi(goodJets, met_phi, minIndex, 4);
			mTBMin = calcClosestMT(goodBJets, met, met_phi);
			
			tree->Fill();
			goodJets.clear();
			goodBJets.clear(); 
		}
	tree->Write();
	outputF->Close(); 
}

