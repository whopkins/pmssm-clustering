import argparse
import pandas as pd
import numpy as np

# All this for reproducibility
import torch
randomSeed = 5
torch.manual_seed(randomSeed)
import random
random.seed(randomSeed)
np.random.seed(randomSeed)
def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    numpy.random.seed(worker_seed)
    random.seed(worker_seed)

gen = torch.Generator()
gen.manual_seed(randomSeed)

from sklearn.preprocessing import MinMaxScaler
import pdb
import collections
import globals

def getPreselections():
    global selections
    selections = collections.OrderedDict()

    selections["1"]="(1)"
    selections["presel_common"]="(passTightCleanDFFlag==1)*(nj_good>=2)*((tcMeTCategory==1)||(tcMeTCategory<0))*(pT_1jet>250)*(num_bjets==0)*((GenFiltMET<100 )|| (RunNumber!=410470))"

    selections["presel_0lep"]=selections['presel_common']+"*(METTrigPassed)*(nsignalLep==0)*(nbaselineLep==0)*(eT_miss>250)*(minDPhi_4jetMET>0.4)"
    
    selections["presel_0lep_1cjet20"]=selections['presel_0lep']+"*(num_cjets20>=1)"
    selections["presel_0lep_1cjet30"]=selections['presel_0lep']+"*(num_cjets30>=1)"
    selections["presel_0lep_1cjet40"]=selections['presel_0lep']+"*(num_cjets40>=1)"
    selections["presel_0lep_2cjet20"]=selections['presel_0lep']+"*(num_cjets20>=2)"
    selections["presel_0lep_2cjet30"]=selections['presel_0lep']+"*(num_cjets30>=2)"
    selections["presel_0lep_2cjet40"]=selections['presel_0lep']+"*(num_cjets40>=2)"

    selections["SRA"]=selections['presel_0lep']+"*(num_cjets20>=2)*(MTcMin20>100)*(m_cc20>150)*(metsigST>5)"

    selections["VRW"] = selections['presel_0lep_2cjet20']+'*(m_cc20>150)*(MTcMin20<100)'
    selections["VRZ"] = selections['presel_0lep_2cjet20']+'*(m_cc20<150)*(MTcMin20>100)'
    
    selections["presel_1lep"]=selections['presel_common']+"*(METTrigPassed)*(nsignalLep==1)*(nbaselineLep==1)*(eT_miss>250)*(minDPhi_4jetMET>0.4)*(pT_1lep>20)"

    selections["presel_2lep"]=selections['presel_common']+"*(nsignalLep==2)*(nbaselineLep==2)*(mll>81)*(mll<101)*(eT_miss<200)*(eT_miss_prime>250)*(minDPhi_4jetMET_prime>0.4)"

    selections["presel_ML"]="(passTightCleanDFFlag==1)*(nj_good>=2)*((tcMeTCategory==1)||(tcMeTCategory<0))*(pT_1jet>250)*(METTrigPassed)*(nsignalLep==0)*(nbaselineLep==0)*(eT_miss>200)"

    selections["presel_ML_1cjet"]="(passTightCleanDFFlag==1)*(nj_good>=2)*((tcMeTCategory==1)||(tcMeTCategory<0))*(pT_1jet>250)*(METTrigPassed)*(nsignalLep==0)*(nbaselineLep==0)*(eT_miss>200)*(num_cjets20>=1)"

def get_DataLoaders(args: argparse.Namespace, trainBranches: list, signals: list=[], preselStr: str='presel_ML', minSize: int=50):

    # Load data
    allSamps = pd.read_hdf(args.data)
    
    # get selection
    if preselStr != '':
        getPreselections()
        presel = selections[preselStr].replace('*', ' & ').replace('||', ' | ').replace('(', '(allSamps.').replace('allSamps.(', '(')
        print(presel)

        # apply selection
        rawDataPresel = allSamps[eval(presel)].copy()
    else:
        print("No preselection specified")
        rawDataPresel = allSamps.copy()

    sampSizes = {}
    sampYields = {}
    sampLabels = sorted(pd.unique(rawDataPresel.sampName))
    print("Number of signal samples:", len(set(sampLabels)))
    selectedIndeces = np.ones((rawDataPresel.shape[0]))
    for sampName in sampLabels:
        sampIndex = rawDataPresel['sampName']==sampName
        sampYield = rawDataPresel[sampIndex].weight.sum()
        sampSize = rawDataPresel[sampIndex].shape[0]
        if sampSize < minSize:
            print("Dropping", sampName, "with size", sampSize)
            #rawDataPresel.drop(index=sampIndex)
            continue
        selectedIndeces = ((selectedIndeces) | (sampIndex))
        sampSizes[sampName] = sampSize
        sampYields[sampName] = sampYield
        #print(sampName, round(sampYield,1), sampIndex.sum())
    print("Total number of events to be clustered:", selectedIndeces.sum())
    selectedData = rawDataPresel[selectedIndeces]        
    # We use max yield when using weights to make sure that the sum
    # of weights for each sample is the same
    # (essentially using weights to scale up all samples) 
    maxYieldKey = max(sampYields, key=lambda k: sampYields[k])
    # The minimum yield is used to sample when we want to avoid using weights.
    minSizeKey = min(sampSizes, key=lambda k: sampSizes[k])

    goodSamps = []
    sampledDataList = []
    print(f"We will make all samples have the following raw event yield: {sampSizes[minSizeKey]}")
    for sampName in sampYields:
        goodSamps.append(sampName)
        sampIndex = selectedData['sampName']==sampName
        sampledDataList.append(selectedData[sampIndex].sample(sampSizes[minSizeKey]))
        selectedData.loc[sampIndex, 'clus_weight'] = selectedData.loc[sampIndex, 'weight']*(sampYields[maxYieldKey]/sampYields[sampName])
        #print(selectedData[sampIndex].clus_weight.sum())
    newRawDataPresel = pd.concat(sampledDataList)
    
    # columns to be used, get rid of 'sampName'
    inputColumns = trainBranches.copy()
    if 'sampName' in inputColumns:
        inputColumns.remove('sampName')

    
    if signals != []:
        print('using only samples: ', signals)
        signalsMap = {}
        for i, signal in enumerate(signals):
            signalsMap[signal] = i
            try:
                signalMask |= newRawDataPresel.sampName == signal
            except:
                signalMask = newRawDataPresel.sampName == signal
        selectedSigs = newRawDataPresel[signalMask]
    else:
        selectedSigs = newRawDataPresel
        signals = sampLabels

    # WH: we should scale only the selected data.
    # scale data: normalize to max value
    scaledData = selectedSigs.copy(deep=True)
    scalers = {}
    maxvalue = 0.
    for column in trainBranches:
        if 'sampName' in column: continue
        scalers[column] = MinMaxScaler()
        scaledData[[column]] = scalers[column].fit_transform(scaledData[[column]].to_numpy())
        # colmax = scaledData[column].max()
    #     if colmax > maxvalue:
    #         maxvalue = colmax
    # scaledData[inputColumns] = scaledData[inputColumns] / maxvalue
    
    # select signal samples
    # data for AE
    aeData = scaledData[trainBranches]
    # WH: we really don't want the sampName as input to the AE.
    #aeSampName = selectedSigs['sampName']

    # I want to make the two samples balanced (similar as above...)
    # I'll calculate the total sum-of-weights for each sample and I'll scale with 1 over it
    norm_weights = {}
    for s in signals:
        sumOfWeights = selectedSigs[selectedSigs.sampName == s].AnalysisWeight.sum()
        norm_weights[s] = sumOfWeights

    # add to new column called norm_weight
    for s in signals:
        norm_weight = norm_weights[s]
        sampIndex = selectedSigs.sampName == s
        selectedSigs.loc[sampIndex, 'norm_weight'] = selectedSigs.loc[sampIndex, 'AnalysisWeight']/(norm_weight)

    # select which weight to be used. In the current implementation of DCN this is not used anyways
    # tempWeights = selectedSigs['AnalysisWeight']
    # tempWeights = selectedSigs['clus_weight']
    tempWeights = selectedSigs['norm_weight']
    # print data characteristics
    print('aeData.shape:', aeData.shape)
    print('aeData.columns:', aeData.columns) 
    print('tempWeights.shape:', tempWeights.shape)
    print('samples:', sampLabels)
    tmp = aeData.apply(lambda s: pd.Series([s.min(), s.max()],index=['min', 'max']))
    print(f'tmp = {tmp}')

    # batch size and test/train fraction
    batch_size = args.batch_size
    testTrainFrac = 0.7
    # config for throughput timing
    # curently vaiable with the available data. 
    # Vangelis: setting when using all the signal samples
    if args.time_throughput:
        batch_size = globals.timing_batch_size = 2**19
        testTrainFrac = 0.9
        args.epoch = globals.timing_epochs = 100
        
    # select events
    nEvents=aeData.shape[0]
    nTrain = np.floor(nEvents*testTrainFrac)
    # use an integer number of batches, i.e. all batches used have the same number of events
    newNTrain = int(np.floor(nTrain/batch_size)*batch_size)
    msk = np.random.choice(nEvents, newNTrain, replace=False)
    
    # train np.arrays
    train_inputs = aeData[inputColumns].to_numpy()[msk]
    #train_targets = aeData['sampName'].map(signalsMap).to_numpy()[msk]

    # test np.arrays
    test_inputs = aeData[inputColumns].to_numpy()[~msk]
    #test_targets = aeData['sampName'].map(signalsMap).to_numpy()[~msk]

    # train & test weights. not used anyways
    trainWeights = torch.Tensor(tempWeights.to_numpy()[msk])
    testWeights = torch.Tensor(tempWeights.to_numpy()[~msk])

    # torch TensorDatasets
    trainDataset = torch.utils.data.TensorDataset(torch.Tensor(train_inputs), torch.Tensor(train_inputs), trainWeights) # create your datset
    testDataset = torch.utils.data.TensorDataset(torch.Tensor(test_inputs), torch.Tensor(test_inputs), testWeights) # create your datset
    
    # torch DataLoaders
    train_loader = torch.utils.data.DataLoader(trainDataset, batch_size=batch_size, worker_init_fn=seed_worker, generator=gen,)
    test_loader = torch.utils.data.DataLoader(testDataset, batch_size=batch_size, worker_init_fn=seed_worker, generator=gen,)

    # We want allSamps and scaledData to analyze it later
    return train_loader, test_loader, allSamps, scaledData, selectedSigs
