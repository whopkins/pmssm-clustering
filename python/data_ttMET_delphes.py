import argparse
import pandas as pd
import numpy as np
import torch
randomSeed = 5
torch.manual_seed(randomSeed)
import random
random.seed(randomSeed)
np.random.seed(randomSeed)
def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    numpy.random.seed(worker_seed)
    random.seed(worker_seed)

gen = torch.Generator()
gen.manual_seed(randomSeed)
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer
import pdb
import collections
import globals

def defScaler(data, args):
    scaler = MinMaxScaler((args['minScale'], args['maxScale']))
    mask = data < args['nanEq']
    data[mask] = -1.*(args['maxScale']-args['minScale'])/args['nBins']
    data[~mask] = scaler.fit_transform(data[~mask].reshape(-1, 1)).flatten()
    return data

def quantScaler(data, args):
    scaler = MinMaxScaler((args['minScale'], args['maxScale']))
    mask = data < args['nanEq']
    data[mask] = -1.*(args['maxScale']-args['minScale'])/args['nBins']
    data[~mask] = scaler.fit_transform(data[~mask].reshape(-1, 1)).flatten()
    
    transformer = QuantileTransformer(n_quantiles=10)
    data[~mask] = transformer.fit_transform(data[~mask].reshape(-1, 1)).flatten()
    return data

def getPreselections():
    # WH: No ttMET selections defined yet
    global selections
    selections = collections.OrderedDict()

    selections["1"]="(1)"
    
def get_DataLoaders(args: argparse.Namespace, trainBranches: list, signals: list=[],
                    preselStr: str='', minSize:int=50,
                    doHist=False, scaler=defScaler,
                    scalerArgs={'minScale':0, 'maxScale':1, 'nanEq':-90, 'nBins':20},
                    reduceData=False,
                    ):
    # nanEq is the value that we store in the ntuples to indicate that there is no data.
    # This tends to be something like -99 or -9999. This needs to be dealt with a bit
    # carefully when rescaling.
    
    # Load data
    tempSamps = []
    sigList = signals
    if sigList == []:
        store = pd.HDFStore(args.data)
        sigList = store.keys()
        store.close()
    
    for signal in sigList:
        tempSamps.append(pd.read_hdf(args.data, key=signal))
    allSamps = pd.concat(tempSamps)

    # get selection
    if preselStr != '':
        getPreselections()
        presel = selections[preselStr].replace('*', ' & ').replace('||', ' | ').replace('(', '(allSamps.').replace('allSamps.(', '(')
        print(presel)

        # apply selection
        rawDataPresel = allSamps[eval(presel)].copy()
    else:
        print("No preselection specified")
        rawDataPresel = allSamps.copy()

    sampSizes = {}
    sampYields = {}
    sampLabels = sorted(pd.unique(rawDataPresel.sampName))
    print("Number of signal samples:", len(set(sampLabels)))
    selectedIndeces = np.ones((rawDataPresel.shape[0]))
    for sampName in sampLabels:
        sampIndex = rawDataPresel['sampName']==sampName
        #sampYield = rawDataPresel[sampIndex].weight.sum()
        sampSize = rawDataPresel[sampIndex].shape[0]
        if sampSize < minSize:
            print("Dropping", sampName, "with size", sampSize)
            #rawDataPresel.drop(index=sampIndex)
            continue
        selectedIndeces = ((selectedIndeces) | (sampIndex))
        sampSizes[sampName] = sampSize
        #sampYields[sampName] = sampYield
        #print(sampName, round(sampYield,1), sampIndex.sum())
    print("Total number of events to be clustered:", selectedIndeces.sum())
    selectedData = rawDataPresel[selectedIndeces]        
    # We use max yield when using weights to make sure that the sum
    # of weights for each sample is the same
    # (essentially using weights to scale up all samples) 
    #maxYieldKey = max(sampYields, key=lambda k: sampYields[k])
    # The minimum yield is used to sample when we want to avoid using weights.
    minSizeKey = min(sampSizes, key=lambda k: sampSizes[k])

    goodSamps = []
    sampledDataList = []
    print(f"We will make all samples have the following raw event yield: {sampSizes[minSizeKey]}")
    for sampName in sampSizes:
        goodSamps.append(sampName)
        sampIndex = selectedData['sampName']==sampName
        sampledDataList.append(selectedData[sampIndex].sample(sampSizes[minSizeKey]))
        #selectedData.loc[sampIndex, 'clus_weight'] = selectedData.loc[sampIndex, 'weight']*(sampYields[maxYieldKey]/sampYields[sampName])
        #print(selectedData[sampIndex].clus_weight.sum())
    newRawDataPresel = pd.concat(sampledDataList)
    
    # columns to be used, get rid of 'sampName'
    inputColumns = trainBranches.copy()
    if 'sampName' in inputColumns:
        inputColumns.remove('sampName')

    
    if signals != []:
        print('using only samples: ', signals)
        signalsMap = {}
        for i, signal in enumerate(signals):
            signalsMap[signal] = i
            try:
                signalMask |= newRawDataPresel.sampName == signal
            except:
                signalMask = newRawDataPresel.sampName == signal
        selectedSigs = newRawDataPresel[signalMask]
    else:
        selectedSigs = newRawDataPresel
        signals = sampLabels

    # WH: we should scale only the selected data.
    # scale data: normalize to max value
    newColumns=[trainBranch for trainBranch in trainBranches]
    newColumns.append('sampName')
    scaledData = selectedSigs[newColumns].copy(deep=True)
    scalers = {}
    
    for column in trainBranches:
        if 'sampName' in column: continue      
        scaledData[column] = scaler(scaledData[column].to_numpy(dtype='float64'), scalerArgs)

    reducedData = scaledData
    if reduceData:
        reducedData = scaledData.copy(deep=True)
        reducedData = reducedData.groupby('sampName').median().reset_index()

    # data for AE
    aeData = scaledData[trainBranches]
    tempWeights = np.ones(selectedSigs.shape)
    # print data characteristics
    print('aeData.shape:', aeData.shape)
    print('aeData.columns:', aeData.columns) 
    print('tempWeights.shape:', tempWeights.shape)
    print('samples:', sampLabels)
    tmp = aeData.apply(lambda s: pd.Series([s.min(), s.max()],index=['min', 'max']))
    print(f'tmp = {tmp}')

    # batch size and test/train fraction
    batch_size = args.batch_size
    testTrainFrac = 0.7
    # config for throughput timing
    # curently vaiable with the available data. 
    # Vangelis: setting when using all the signal samples
    if args.time_throughput:
        batch_size = globals.timing_batch_size = 2**19
        testTrainFrac = 0.9
        args.epoch = globals.timing_epochs = 100
        
    # select events
    nEvents=aeData.shape[0]
    nTrain = np.floor(nEvents*testTrainFrac)
    # use an integer number of batches, i.e. all batches used have the same number of events
    newNTrain = int(np.floor(nTrain/batch_size)*batch_size)
    msk = np.random.choice(nEvents, newNTrain, replace=False)
    
    # train np.arrays
    train_inputs = aeData[inputColumns].to_numpy()[msk]
    #train_targets = aeData['sampName'].map(signalsMap).to_numpy()[msk]

    # test np.arrays
    test_inputs = aeData[inputColumns].to_numpy()[~msk]
    #test_targets = aeData['sampName'].map(signalsMap).to_numpy()[~msk]

    # train & test weights. not used anyways
    #trainWeights = torch.Tensor(tempWeights.to_numpy()[msk])
    #testWeights = torch.Tensor(tempWeights.to_numpy()[~msk])

    trainWeights = torch.Tensor(tempWeights[msk])
    testWeights = torch.Tensor(tempWeights[~msk])

    # torch TensorDatasets
    trainDataset = torch.utils.data.TensorDataset(torch.Tensor(train_inputs), torch.Tensor(train_inputs), trainWeights) # create your datset
    testDataset = torch.utils.data.TensorDataset(torch.Tensor(test_inputs), torch.Tensor(test_inputs), testWeights) # create your datset
    
    # torch DataLoaders
    train_loader = torch.utils.data.DataLoader(trainDataset, batch_size=batch_size,
                                               worker_init_fn=seed_worker, generator=gen, drop_last=True)
    test_loader = torch.utils.data.DataLoader(testDataset, batch_size=batch_size,
                                              worker_init_fn=seed_worker, generator=gen, drop_last=True)

    # We want allSamps and scaledData to analyze it later
    return train_loader, test_loader, allSamps, scaledData, selectedSigs, reducedData

if __name__ == '__main__':
    # This is for rapid testing
    args = argparse.Namespace()
    args.data = '/users/whopkins/sigclustering/sigclustering/python/ttMET_delphes_sepSigs.h5'
    args.time_throughput = False
    args.batch_size = 128
    trainBranches = ['MET', 'pT_4jet']
    varListStr = '_'.join(trainBranches)
    signals = ['sig_1000_1', 'sig_500_200']
    trainDataLoader, testDataLoader, allData, scaledData, selectedSigs, reducedData = get_DataLoaders(args, trainBranches, signals=signals, minSize=1000, scaler=quantScaler, reduceData=True)
    print(scaledData)
    print(reducedData)
    
