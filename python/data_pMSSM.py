import argparse
import pandas as pd
import numpy as np
import torch
randomSeed = 5
torch.manual_seed(randomSeed)
import random
random.seed(randomSeed)
np.random.seed(randomSeed)
def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    numpy.random.seed(worker_seed)
    random.seed(worker_seed)

gen = torch.Generator()
gen.manual_seed(randomSeed)
from sklearn.preprocessing import MinMaxScaler, QuantileTransformer
import pdb
import collections
import globals

def get_DataLoaders(args: argparse.Namespace, trainBranches: list, signals: list=[],
                    preselStr: str='', minSize:int=50,
                    doHist=False, 
                    reduceData=False,
                    reduction='mean'
                    ):
    # nanEq is the value that we store in the ntuples to indicate that there is no data.
    # This tends to be something like -99 or -9999. This needs to be dealt with a bit
    # carefully when rescaling.
    
    # Load data
    tempSamps = []
    sigList = signals
    if sigList == []:
        store = pd.HDFStore(args.data)
        sigList = store.keys()
        store.close()
    
    for signal in sigList:
        tempData = pd.read_hdf(args.data, key=signal)
        tempSamps.append(tempData)
    allSamps = pd.concat(tempSamps)
    # get selection
    if preselStr != '':
        getPreselections()
        presel = selections[preselStr].replace('*', ' & ').replace('||', ' | ').replace('(', '(allSamps.').replace('allSamps.(', '(')
        print(presel)

        # apply selection
        rawDataPresel = allSamps[eval(presel)].copy()
    else:
        print("No preselection specified")
        rawDataPresel = allSamps.copy()

    sampSizes = {}
    sampYields = {}
    sampLabels = sorted(pd.unique(rawDataPresel.sampName))
    print("Number of signal samples:", len(set(sampLabels)))
    selectedIndeces = np.ones((rawDataPresel.shape[0]))
    for sampName in sampLabels:
        sampIndex = rawDataPresel['sampName']==sampName
        #sampYield = rawDataPresel[sampIndex].weight.sum()
        sampSize = rawDataPresel[sampIndex].shape[0]
        if sampSize < minSize:
            print("Dropping", sampName, "with size", sampSize)
            #rawDataPresel.drop(index=sampIndex)
            continue
        selectedIndeces = ((selectedIndeces) | (sampIndex))
        sampSizes[sampName] = sampSize
        #sampYields[sampName] = sampYield
        #print(sampName, round(sampYield,1), sampIndex.sum())
    print("Total number of events to be clustered:", selectedIndeces.sum())
    selectedData = rawDataPresel[selectedIndeces]
    minSizeKey = min(sampSizes, key=lambda k: sampSizes[k])

    goodSamps = []
    sampledDataList = []
    print(f"We will make all samples have the following raw event yield: {sampSizes[minSizeKey]}")
    for sampName in sampSizes:
        goodSamps.append(sampName)
        sampIndex = selectedData['sampName']==sampName
        sampledDataList.append(selectedData[sampIndex].sample(sampSizes[minSizeKey]))
        #selectedData.loc[sampIndex, 'clus_weight'] = selectedData.loc[sampIndex, 'weight']*(sampYields[maxYieldKey]/sampYields[sampName])
        #print(selectedData[sampIndex].clus_weight.sum())
    newRawDataPresel = pd.concat(sampledDataList)
    
    # columns to be used, get rid of 'sampName'
    inputColumns = trainBranches.copy()
    if 'sampName' in inputColumns:
        inputColumns.remove('sampName')

    
    if signals != []:
        print('using only samples: ', signals)
        signalsMap = {}
        for i, signal in enumerate(signals):
            signalsMap[signal] = i
            try:
                signalMask |= newRawDataPresel.sampName == signal
            except:
                signalMask = newRawDataPresel.sampName == signal
        selectedSigs = newRawDataPresel[signalMask]
    else:
        selectedSigs = newRawDataPresel
        signals = sampLabels

    # WH: we should scale only the selected data.
    # scale data: normalize to max value
    newColumns=[trainBranch for trainBranch in trainBranches]
    newColumns.append('sampName')
    
    reducedData = selectedSigs[newColumns].copy(deep=True)
    if reduceData:
        if reduction == 'median':
            reducedData = reducedData.groupby('sampName').median().reset_index()
        else:
            reducedData = reducedData.groupby('sampName').mean().reset_index()

    # We want allSamps and scaledData to analyze it later
    return allSamps, selectedSigs, reducedData

if __name__ == '__main__':
    # This is for rapid testing
    args = argparse.Namespace()
    args.data = '/users/whopkins/sigclustering/sigclustering/python/pMSSM.h5'
    args.time_throughput = False
    args.batch_size = 128
    trainBranches = ['MET', 'pT_4jet']
    varListStr = '_'.join(trainBranches)
    signals = []
    allData, selectedSigs, reducedData = get_DataLoaders(args, trainBranches, signals=signals, minSize=1000, reduceData=True)
    print(selectedSigs)
    print(reducedData)
    print(allData.shape)
    
