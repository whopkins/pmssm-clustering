import pickle
pickle.HIGHEST_PROTOCOL = 4
import glob, pandas, uproot, re, argparse

def convertToHDF(path, outFName, treeName='Nominal', branches=[], verbose=False):
    paths = glob.glob(path+'stop2topLSP*flat*.root')
    branches = []
    data = {}
    preCut = '1'
    if verbose:
        print('\nSamples that will be stored.')
        
    for path in paths:
        sampName = 'sig_'+path.split('/')[-1].split('stop2topLSP_')[1].split('_flat')[0]
        tree = uproot.open(path)[treeName]
        if verbose and path == paths[0]:
            tree.show()
        if len(branches) > 0:
            data[sampName] = tree.arrays(branches, library='pd')
        else:
            data[sampName] = tree.arrays(library='pd')

        data[sampName]['sampName'] = sampName

        if verbose:
            print(sampName)

        data[sampName].to_hdf(outFName, key=sampName, mode='a')
    #allSamps = pandas.concat(list(data.values()))
    #sampLabels = pandas.unique(allSamps.sampName)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-p', '--path', default='/data/atlasfs02/c/users/whopkins/stop2LSP_delphes/')
    #parser.add_argument('-d', '--datasetName', default='ttMET_delphes')
    parser.add_argument('-o', '--outFName', default='ttMET_delphes.h5')
    parser.add_argument('-v', '--verbose', action='store_true')
    args = parser.parse_args()

    #convertToHDF(args.path, args.outFName, args.datasetName, verbose=args.verbose)
    convertToHDF(args.path, args.outFName, verbose=args.verbose)

