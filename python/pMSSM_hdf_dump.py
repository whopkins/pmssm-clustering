import pickle
#pickle.HIGHEST_PROTOCOL = 4
import os
os.environ['OPENBLAS_NUM_THREADS'] = '1'
import glob, pandas, uproot, re, argparse, awkward, math, gc, csv, os
import numpy as np

def convertToHDF(path, outFName, treeName='Nominal', branches=[],
                 verbose=False, startFile=0, endFile=-1, random_state=5,
                 crossSectionPath='ewk_models_for_walter.csv',
                 sampNamePrefix='ew_',
                 minMaxOnly=False,
                 redoMinMax=False,
                 minMaxFName="minsAndScales_EW.pkl"):

    if endFile > 0:
        paths = glob.glob(path+'*.root')[startFile:endFile]
    else:
        paths = glob.glob(path+'*.root')[startFile:]

    if len(branches) > 0:
        branchesUsed = branches
    else:
        print("Loading one file to get branches")
        tree = uproot.open(paths[0])[treeName]
        tempData = tree.arrays(library='pd', entry_stop=1)
        # Temporarily excluded the MC weights since they are an array
        branchesUsed = [branch for branch in tempData.keys() if branch != "weights"]

    cross_sections = {}
    excluded = {}
    print("Getting cross sections")
    with open(crossSectionPath, newline='') as csvfile:
        modelInfoReader = csv.DictReader(csvfile)
        for row in modelInfoReader:
            modelNum = row['modelName'].replace(".0", "")
            modelNum = modelNum.zfill(6)[0]+'_'+modelNum
            cross_sections[sampNamePrefix+modelNum] = float(row['Cross_section_EW'])
            excluded[sampNamePrefix+modelNum] = int(row['Excluded'])
            
    
    minIgnoreBranches = ['channel', 'clustering', 'event', 'weight']
    cutBranches = [branch for branch in branchesUsed if all([ignore not in branch.lower() for ignore in minIgnoreBranches])]

    print("Getting min and max values")
    if not os.path.exists(minMaxFName) or redoMinMax or minMaxOnly:
        print("Getting number of entries for each model")
        from ROOT import TChain, gDirectory, TFile
        chain = TChain(treeName)
        minEntries = 9999999999999
        for path in paths:
            chain.Add(path)
            tempFile = TFile.Open(path)
            tempTree = tempFile.Get(treeName)
            entries = tempTree.GetEntries()
            if minEntries > entries:
                minEntries = entries
            tempFile.Close()
        
        print("All samples will have the following number of events", minEntries)

        initVal = -9990
        scales = {}
        mins = {}
        branchesToConsider = []
        for branch in cutBranches:
            cut = branch+">"+str(initVal)
            print("Copying tree to calculate minima and maxima with the following cut\n", cut)
            minTree = list(uproot.concatenate([path+":"+treeName for path in paths], branch, cut, library='np').values())[0]
            print("Done copying tree which now has", minTree.shape[0], "entries")

            if minTree.shape[0] == 0:
                continue;                

            minVal = np.min(minTree)
            maxVal = chain.GetMaximum(branch)
            # I know I calculate these but I will use my physics knowledge to have the
            # max and mins be round numbers
            if (abs(maxVal-minVal) < 0.0001):
                continue;
            elif 'dphi' in branch:
                mins[branch] = 0
                scales[branch] = math.pi
            elif 'phi' in branch:
                mins[branch] = -1*math.pi
                scales[branch] = 2*math.pi
            # elif 'eta' in branch:
            #     mins[branch] = -2.8
            #     scales[branch] = 2*2.8
            else:
                mins[branch] = minVal
                scales[branch] = maxVal-minVal
            branchesToConsider.append(branch)
            
        print("The following branches will be considered", branchesToConsider)
        print("Writing minima and maxima to file")
        pickle.dump([mins,scales, minEntries], open(minMaxFName, "wb"))
        print("Done writing minima and maxima to file")
        if minMaxOnly:
            return
    else:
        [mins,scales, minEntries] = pickle.load(open(minMaxFName, "rb"))
        print(mins.keys())
    
    naFill = -0.1
    # We want at least 10K events
    if minEntries < 10000:
        minEntries = 10000
    for path in paths:
        print("Loading sample")
        sampName = sampNamePrefix+'_'.join(path.split('/')[-1].rstrip(".root").split('_')[-3:])
        if sampName not in cross_sections:
            continue
        tree = uproot.open(path)[treeName]
        if verbose and path == paths[0]:
            tree.show()
        if len(branches) > 0:
            data = tree.arrays(branches, library='pd')
        else:
            data = tree.arrays(branchesUsed, library='pd')
        print("Loaded", data.shape, "samples")
        if data.shape[0] < minEntries:
            print("Omitting sample", sampName, "because it had less than", minEntries, "events")
            continue
        
        # This is still needed despite is only "loading" minEntries from the tree.
        # This is because of bucket size limitations
        print("Sampling down to", minEntries)
        sampledData = data.sample(minEntries, random_state=random_state)
        sampledData = sampledData.astype('float32')
        print("Rescaling")
        for branch in cutBranches:
            #print("Rescaling", branch)
            # Need to check for the -99999 values and replace them with nan before scaling
            filled = (sampledData[branch] > -999999) & (sampledData[branch] < 999999)
            sampledData.loc[filled,[branch]] = 1.*(sampledData.loc[filled,[branch]] - mins[branch])/scales[branch]
            sampledData.loc[~filled,[branch]] = naFill#np.nan

        sampledData['sampName'] = sampName
        sampledData['cross_section'] = 1.
        sampledData['excluded'] = 1
        
        if sampName in cross_sections:
            sampledData['cross_section'] = cross_sections[sampName]
            sampledData['excluded'] = excluded[sampName]
        
        print("Done processing "+path)
        # print(sampledData.min())
        # print(sampledData.max())
        sampledData.to_hdf(outFName, key='pMSSM', append=True, mode='a', min_itemsize={ 'sampName' : 50 })
        del data
        gc.collect()
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-p', '--path', default='/data/atlasfs02/c/users/whopkins/pMSSM/flat_truthNtuples_2022-10-31/')
    parser.add_argument('-o', '--outFName', default='pMSSM.h5')
    parser.add_argument('-v', '--verbose', action='store_true')
    parser.add_argument('-t', '--treeName', default='Nominal')
    parser.add_argument('--startFile', type=int, default=0)
    parser.add_argument('--endFile', type=int, default=-1)
    parser.add_argument('--minMaxOnly', action='store_true')
    parser.add_argument('--redoMinMax', action='store_true')
    parser.add_argument('--minMaxFName', default='minsAndScales_EW.pkl')
    parser.add_argument('--crossSectionPath', default='ewk_models_for_walter.csv')
    parser.add_argument('--sampNamePrefix', default='ew_')

    args = parser.parse_args()

    convertToHDF(args.path, args.outFName, treeName=args.treeName, verbose=args.verbose,
                 startFile=args.startFile, endFile=args.endFile,
                 crossSectionPath=args.crossSectionPath,
                 sampNamePrefix=args.sampNamePrefix,
                 minMaxOnly=args.minMaxOnly,
                 redoMinMax=args.redoMinMax,
                 minMaxFName=args.minMaxFName)

