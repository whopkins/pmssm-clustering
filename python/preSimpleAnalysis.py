#!/usr/bin/env python
import glob, argparse
from ROOT import TH1F, TFile, TCanvas, gROOT

def plot_kinematics(inputPath, outPrefix):
    gROOT.SetBatch(True)
    inF = TFile.Open(inputPath)
    tree = inF.Get('ntuple')

    varNames = [
        'jet_pt[0]',
        '@jet_pt.size()',
        'met_pt',
        'el_pt[0]',
        '@el_pt.size()',
        'mu_pt[0]',
        '@mu_pt.size()',
        'ph_pt[0]',
        '@ph_pt.size()',
        'tau_pt[0]',
        '@tau_pt.size()',

    ]
    binning={
        'jet_pt[0]':[20,0,1000],
        '@jet_pt.size()':[10,0,10],
        'met_pt':[20,0,1000],
        'el_pt[0]':[20,0,200],
        '@el_pt.size()':[5,0,5],
        'mu_pt[0]':[20,0,200],
        '@mu_pt.size()':[5,0,5],
        'ph_pt[0]':[20,0,400],
        '@ph_pt.size()':[5,0,5],
        'tau_pt[0]':[20,0,400],
        '@tau_pt.size()':[5,0,5],

    }
    can = TCanvas('c', '', 800,600)
    for varName in varNames:
        hist = TH1F('tempHist', '', *binning[varName])
        tree.Draw(varName+'>>tempHist')
        can.Clear();
        hist.Draw()
        cleanVarName = varName.replace('[','').replace(']','').replace('_pt.size()', ' multiplicity').replace('@','')
        xLabel = cleanVarName
        if 'multiplicity' not in cleanVarName:
            xLabel +=' [GeV]'
        hist.GetXaxis().SetTitle(xLabel)
        hist.GetYaxis().SetTitle('Events')
                
        can.Print(cleanVarName.replace(' ', '_')+outPrefix+'.pdf')
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-p', '--path', default='')
    parser.add_argument('--outPrefix', default='')
    
    args = parser.parse_args()

    plot_kinematics(args.path, outPrefix=args.outPrefix)

