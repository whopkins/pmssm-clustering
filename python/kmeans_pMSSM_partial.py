#!/usr/bin/env python
import glob, matplotlib, pandas, argparse, sys, pickle, multiprocessing, os, psutil, gc, copy, pathlib, scipy
randomState = 5
import numpy as np
np.random.seed(randomState)
from sklearn.cluster import KMeans, MiniBatchKMeans
import matplotlib.pyplot as plt
plt.style.use('default')
font = {'size':14}
matplotlib.rc('font', **font)

process = psutil.Process(os.getpid())

def kmeans_fit(kmeans, data, weights):
    kmeans.partial_fit(data, sample_weight=weights)
    return kmeans

def kmeans_predict(kmeans, data, weights):
    labels = kmeans.predict(data, sample_weight=weights)
    return labels

def calculate_dispersion(data: np.ndarray, centroids: np.ndarray, labels: np.ndarray) -> float:
        """
        Calculate the dispersion between actual points and their assigned centroids
        Taken from: https://github.com/milesgranger/gap_statistic/blob/master/gap_statistic/optimalK.py
        """
        disp = np.sum(
            np.sum(
                [np.abs(inst - centroids[label]) ** 2 for inst, label in zip(data, labels)]
            )
        )
        return disp

def get_ref_dispersions(scaled_mins, scaled_maxs, mins, scales, trainBranches, branchDType,
                        n_refs, randomState, batch_size, start_k, k_max, fName='ref_dispersions.pkl',
                        floatNPoints=15):
    ref_kmeans_instances = [MiniBatchKMeans(n_init=3, n_clusters=k, batch_size=batch_size, random_state=randomState) for k in range(start_k,k_max+1)]
    
    ref_dispersions = [[] for k in range(start_k,k_max+1)]
    for i in range(n_refs):
        # We will make sure that our reference data is distributed uniformly but with
        # similar characteristics as our real data, i.e., integers are treated differently.
        # We will choose 20 points per float dimension. Remember that the dimensionality of the problem can make this large
        # very quickly. This really only affects the gap statistic.
        points = []
        for bI, branch in enumerate(trainBranches):
            if branchDType[bI] == 'int':
                nPoints = int(scales[branch])+1
                points.append(((np.random.randint(0, nPoints, size=nPoints)-mins[branch])/scales[branch]).astype('float16'))
            else:
                tempPoints = np.random.uniform(scaled_mins[bI], scaled_maxs[bI], size=floatNPoints).astype('float16')
                # If a value is negative we set it to -0.1 because this is what we do in the real data
                tempPoints[tempPoints < 0] = -0.1
                points.append(tempPoints.astype('float16'))
        grid = np.meshgrid(*points)
        ravel = [xx.ravel() for xx in grid]
        random_data = np.swapaxes(np.c_[ravel], 0, 1)
        ref_weights = np.ones((random_data.shape[0]))
        fit_args = [[kmeans, random_data, ref_weights] for kmeans in ref_kmeans_instances]
        with multiprocessing.Pool(processes=k_max-1) as pool:
            ref_kmeans_instances = pool.starmap(kmeans_fit, fit_args)

        disp_args = [[random_data, kmeans.cluster_centers_, kmeans.labels_] for kmeans in ref_kmeans_instances]
        with multiprocessing.Pool(processes=k_max-1) as pool:
            dispersions = pool.starmap(calculate_dispersion, disp_args)
        for k_index, dispersion in enumerate(dispersions):
            ref_dispersions[k_index].append(dispersion)
        
    with open(fName, 'wb') as handle:
        pickle.dump(ref_dispersions, handle)
    return ref_dispersions

def calculate_gap(dispersions, ref_dispersions, start_k, k_max):
    # Calculate gap statistic
    gaps = []
    sdks = []
    sks = []
    gaps_star = []
    sdks_star = []
    sks_star = []
    for dispersion, ref_dispersion in zip(dispersions, ref_dispersions):
        n_refs = len(ref_dispersion)
        ref_log_dispersion = np.mean(np.log(ref_dispersion))
        log_dispersion = np.log(dispersion)
        gap_value = ref_log_dispersion - log_dispersion
        # compute standard deviation
        sdk = np.sqrt(np.mean((np.log(ref_dispersion) - ref_log_dispersion) ** 2.0))
        sk = np.sqrt(1.0 + 1.0 / n_refs) * sdk

        gaps.append(gap_value)
        sdks.append(sdk)
        sks.append(sk)
        # Calculate Gap* statistic
        # by "A comparison of Gap statistic definitions with and
        # with-out logarithm function"
        # https://core.ac.uk/download/pdf/12172514.pdf
        gap_star = np.mean(ref_dispersion) - dispersion
        sdk_star = np.sqrt(np.mean((ref_dispersion - dispersion) ** 2.0))
        sk_star = np.sqrt(1.0 + 1.0 / n_refs) * sdk_star

        gaps_star.append(gap_star)
        sdks_star.append(sdk_star)
        sks_star.append(sk_star)

    gap_df = pandas.DataFrame({"n_clusters": [start_k+i for i in range(k_max-1)],
                           "gap_value":gaps,
                           "sdk":sdks,
                           "sk":sks,
                           "gap*":gaps_star,
                           "sk*":sks_star,
                           })
    gap_df["gap_k+1"] = gap_df["gap_value"].shift(-1)
    gap_df["gap*_k+1"] = gap_df["gap*"].shift(-1)
    gap_df["sk+1"] = gap_df["sk"].shift(-1)
    gap_df["sk*+1"] = gap_df["sk*"].shift(-1)
    gap_df["diff"] = gap_df["gap_value"] - gap_df["gap_k+1"] + gap_df["sk+1"]
    gap_df["diff*"] = gap_df["gap*"] - gap_df["gap*_k+1"] + gap_df["sk*+1"]
    return gap_df
    
def perform_fit(dataFilePaths, start_k, k_max, batch_size, trainBranches,
                weightStr="cross_section", fName='partialClustering.pkl',
                excluded=-1, removeZeros=False, minTrainVal={},
                minCrossSec=0.1, maxCrossSec=500):
    kmeans_instances = [MiniBatchKMeans(n_init=3, n_clusters=k, batch_size=batch_size, random_state=randomState) for k in range(start_k,k_max+1)]
    for path in dataFilePaths:
        if excluded == 0 or excluded == 1:
            data = pandas.read_hdf(path, columns=trainBranches+['cross_section', 'excluded'])
            data = data[data.excluded==excluded]
        else:
            data = pandas.read_hdf(path, columns=trainBranches+['cross_section'])

        data = data[data.cross_section > minCrossSec]
        
        # WH: when analyzing clusters formed from excluded models we want to get rid of models with massive cross sections
        # This will make it easier to compare to existing analyses.
        if excluded == 1:
            data = data[data.cross_section < maxCrossSec]

        if removeZeros:
            index = (data[trainBranches[0]] <= minTrainVal[trainBranches[0]])
            for branch in trainBranches[1:]:
                index = (index) & (data[branch] <= minTrainVal[branch])
            data=data[~index]

        if data.shape[0] == 0:
            continue
        
        if weightStr =="":
            weights = np.ones((data.shape[0]))
        else:
            weights = data[weightStr]
        fit_args = [[kmeans, data[trainBranches].to_numpy(), weights] for kmeans in kmeans_instances]
        with multiprocessing.Pool(processes=k_max-1) as pool:
              kmeans_instances = pool.starmap(kmeans_fit, fit_args)
        #print(round(process.memory_info().rss/ 1024 ** 2))
        
    with open(fName, 'wb') as handle:
        pickle.dump(kmeans_instances, handle)
    return kmeans_instances

def predict(dataFilePaths, kmeans_instances, trainBranches, weightStr="cross_section",
            disp_fName='dispersions.pkl', labels_fName="labels.h5",
            excluded=-1, removeZeros=False,
            minTrainVal={}, minCrossSec=0.1, maxCrossSec=500):
    dispersions = [0 for kmeans in kmeans_instances]
    labels = []
    for path in dataFilePaths:
        if excluded == 0 or excluded == 1:
            data = pandas.read_hdf(path, columns=trainBranches+['cross_section', 'excluded'])
            data = data[data.excluded==excluded]
        else:
            data = pandas.read_hdf(path, columns=trainBranches+['cross_section'])
        data = data[data.cross_section > minCrossSec]
        if excluded == 1:
            data = data[data.cross_section < maxCrossSec]
        if removeZeros:
            index = (data[trainBranches[0]] <= minTrainVal[trainBranches[0]])
            for branch in trainBranches[1:]:
                index = (index) & (data[branch] <= minTrainVal[branch])
            data=data[~index]

        if data.shape[0] == 0:
            continue
        
        if weightStr =="":
            weights = np.ones((data.shape[0]))
        else:
            weights = data[weightStr]
        fit_data = data[trainBranches].to_numpy()
        fit_args = [[kmeans, fit_data, weights] for kmeans in kmeans_instances]
        with multiprocessing.Pool(processes=len(kmeans_instances)) as pool:
              temp_labels = pool.starmap(kmeans_predict, fit_args)
        labels_df = pandas.DataFrame(np.array(temp_labels).T).astype('uint8')

        disp_args = [[fit_data, kmeans.cluster_centers_, sub_labels] for sub_labels, kmeans in zip(temp_labels, kmeans_instances)]
        with multiprocessing.Pool(processes=len(kmeans_instances)) as pool:
            temp_dispersions = pool.starmap(calculate_dispersion, disp_args)
            
        for ilabels, disp in enumerate(temp_dispersions):
            dispersions[ilabels]+=disp
        labels_df.to_hdf(labels_fName, key='clus_labels', append=True, mode='a')
        labels.append(labels_df)
        
    labels = pandas.concat(labels)
    with open(disp_fName, 'wb') as handle:
        pickle.dump(dispersions, handle)
        
    return labels, dispersions

def getClusIndeces(clusI, clusLabels):
    clusIndex = (clusLabels==clusI)
    return clusIndex
    
def findClusFracs(sampName, sampNameData, bestNClus, clusIndeces):
    highestFrac = -99
    bestClus = -1
    clusFracs = []
    sampIndex = sampNameData==sampName
    for clusI in range(bestNClus):
        clusIndex = clusIndeces[:,clusI]
        sampClusIndex = (sampIndex) & (clusIndex)
        clusFrac = 1.*np.sum(sampClusIndex)/np.sum(sampIndex)
        clusFracs.append(clusFrac)
        if clusFrac > highestFrac:
            highestFrac = clusFrac
            bestClus = clusI
    
    frac_result = [bestClus, highestFrac, clusFracs]    
    return frac_result

def findClusYields(sampName, sampNameData, bestNClus, clusIndeces, crossSections, lumi, sampTotalEvents):
    highestYield = -99
    bestClus = -1
    clusYields = []
    sampIndex = sampNameData==sampName
    
    for clusI in range(bestNClus):
        clusIndex = clusIndeces[:,clusI]
        sampClusIndex = (sampIndex) & (clusIndex)
        # This might still be wrong. We need the number of events before any selection
        clusYield = crossSections[sampClusIndex].iloc[0]*lumi*(np.sum(sampClusIndex)/sampTotalEvents)
        clusYields.append(clusYield)
        if clusYield > highestYield:
            highestYield = clusYield
            bestClus = clusI
    yield_result = [bestClus, highestYield, clusYields]    
    return yield_result
            
def calc_reduction(data, clusIndex):
    """
    Calculate a reduction (mean, standard deviation, median, etc) for subset of data for a given number of clusters
    """
    indivClusData = data.loc[clusIndex].to_numpy()
    ref = np.mean(indivClusData)
    lowErr = np.std(indivClusData)
    highErr = lowErr
    # ref = np.median(indivClusData)
    # lowErr = ref-np.quantile(indivClusData, 0.16)
    # highErr = np.quantile(indivClusData, 0.84)-ref
    return [ref, lowErr, highErr]
    
def cluster(dataPath, redo_kmeans, redo_labels, redo_refs, redo_cFracs, redo_cYields, redo_cInd,
            minMaxFName, endFile=-1, start_k=2, k_max=20, n_refs=3,
            plotErrors=False, plotDists=False,
            bestNClus=3, outPrefix="", outDir="./",
            weightStr='',
            excluded=-1, removeZeros=False,
            floatNPoints=15, minCrossSec=0.1, maxCrossSec=500,
            lumi=140):

    if outDir != "":
        pathlib.Path(outDir).mkdir(parents=True, exist_ok=True)
        pathlib.Path(outDir+'/plots').mkdir(parents=True, exist_ok=True)

    if endFile > 0:
        dataFilePaths = sorted(glob.glob(dataPath)[:endFile])
    else:
        dataFilePaths = sorted(glob.glob(dataPath))

    # This is so we can interpret the clustering result with normal scales
    with open(minMaxFName, "rb") as minMaxFile:
        all_mins, all_scales, minEvents = pickle.load(minMaxFile)

    #trainBranches = ['nj_good', 'num_bjets', 'nLep_signal', 'MET', 'pT_1jet', 'pT_1bjet', 'pT_1lep',]
    #trainBranches = ['MET', 'pT_1jet', 'pT_4jet', 'pT_1bjet', 'pT_1lep',]
    trainBranches = ['MET', 'pT_1jet', 'pT_1bjet', 'pT_1lep', 'HT', 'HTb', 'LT',
                     #'cross_section',
                     ]
    obsBranches = ['nj_good', 'num_bjets', 'nLep_signal',
                   'pT_4jet',
                   #'HT', 'HTb', 'LT',
                   ]
    
    minTrainVal = {'nj_good':0., 'num_bjets':0., 'nLep_signal':0.,
                   #'MET':200., 'pT_1jet':200., 'pT_4jet':100., 'pT_1bjet':100., 'pT_1lep':20.,
                   'MET':200., 'pT_1jet':500., 'pT_4jet':0., 'pT_1bjet':100., 'pT_1lep':20.,
                   'HT':1000., 'LT':0., 'HTb':0.,
                   #'cross_section':0,
                   }
    mins = dict((k, all_mins[k]) for k in minTrainVal.keys())
    scales = dict((k, all_scales[k]) for k in minTrainVal.keys())

    for branch in minTrainVal:
        minTrainVal[branch] = (minTrainVal[branch]-mins[branch])/scales[branch]
    branchDType = [#'float', 'float', 
                   'float', 'float', 'float', 'float', 'float', 'float', 'float',
                   #'float',
                   ]    
   
    testData = pandas.concat([pandas.read_hdf(path) for path in dataFilePaths[:5]])
    print("Objects stored in data:\n", testData.keys())
    print("Starting with", testData.shape[0], "events")
    # This is to remove excluded models
    excludedStr=''
    testData = testData[testData.cross_section>minCrossSec]
    print(f"After removing models with cross section less than {minCrossSec} fb, we have", testData.shape[0], "events")
    
      
    if excluded == 0 or excluded == 1:
        print(f"Removing models with excluded!={excluded}. Starting with", testData.shape[0], "events")
        testData=testData[testData.excluded==excluded]
        print("Now have", testData.shape[0], "events")
        excludedStr = f'_excluded_{excluded}'
        if excluded == 1:
            print(f"Removing excluded models with cross sections above {maxCrossSec} fb. Starting with", testData.shape[0], "events")
            testData = testData[testData.cross_section<maxCrossSec]
            print("Now have", testData.shape[0], "events")

    
    zeroStr=''
    if removeZeros:
        print("Removing events that have no observables. Starting with", testData.shape[0], "events")
        index = (testData[trainBranches[0]] <= minTrainVal[trainBranches[0]])
        for branch in trainBranches[1:]:
            index = (index) & (testData[branch] <= minTrainVal[branch])
        testData=testData[~index]
        print("Now have", testData.shape[0], "events")
        zeroStr = '_noZeros'

    print("Read in following file as test files:", dataFilePaths[:5])
    print("Checking that the rescaling worked correctly for pT_1jet: \n", testData['pT_1jet'].min(), "\n", testData['pT_1jet'].max())
    print("Data shape:", testData.shape)

    # The data is scaled to have known mins and maxes but we will still get the minima from the test data
    # This is slightly dangerous since out test data could have large values that are not representative of
    # full data set
    scaled_mins = testData[trainBranches].to_numpy().min(axis=0)
    if np.sum(scaled_mins > 0.1) > 0:
        print("You got some large minima")
    scaled_maxs = np.ones(scaled_mins.shape)

    batch_size=testData.shape[0]
    varListStr = '_'.join(trainBranches)

    kmeans_fName = f'{outDir}/{outPrefix}_kmeans{excludedStr}{zeroStr}__{varListStr}.pkl'
    if os.path.exists(kmeans_fName) and redo_kmeans == False:
        print("Loading KMeans instances from file")
        with open(kmeans_fName, "rb") as kmeans_file:
            kmeans_instances = pickle.load(kmeans_file)
    else:
        print("Performing fit in parts. This may take a long time.")
        kmeans_instances = perform_fit(dataFilePaths, start_k, k_max, batch_size,
                                       trainBranches, fName=kmeans_fName,
                                       excluded=excluded, removeZeros=removeZeros,
                                       minTrainVal=minTrainVal, minCrossSec=minCrossSec,
                                       maxCrossSec=maxCrossSec, weightStr=weightStr)

    labels_fName = f'{outDir}/{outPrefix}_labels{excludedStr}{zeroStr}__{varListStr}.h5'
    disp_fName = f'{outDir}/{outPrefix}_disp{excludedStr}{zeroStr}__{varListStr}.pkl'
    if os.path.exists(labels_fName) and os.path.exists(disp_fName) and redo_labels == False:
        print("Loading labels and dispersion for all data.")
        with open(disp_fName, "rb") as disp_file:
            dispersions = pickle.load(disp_file)
        labels = pandas.read_hdf(labels_fName, key='clus_labels')
    else:
        print("Getting labels and dispersions for the full data set. This may take a bit.")
        labels, dispersions = predict(dataFilePaths, kmeans_instances, trainBranches,
                                      labels_fName=labels_fName, disp_fName=disp_fName,
                                      excluded=excluded, removeZeros=removeZeros,
                                      minTrainVal=minTrainVal, minCrossSec=minCrossSec,
                                      maxCrossSec=maxCrossSec,
                                      weightStr=weightStr)

    ref_fName = f'{outDir}/{outPrefix}_ref{excludedStr}{zeroStr}_{varListStr}.pkl'
    if os.path.exists(ref_fName) and redo_refs == False:
        print("Loading reference dispersion from file.")
        with open(ref_fName, "rb") as ref_file:
            ref_dispersions = pickle.load(ref_file)
    else:
        print("Evaluating the dispersions for uniform reference data sets")
        ref_dispersions = get_ref_dispersions(scaled_mins, scaled_maxs, mins, scales, trainBranches, branchDType,
                                              n_refs, randomState,
                                              batch_size, start_k, k_max, fName=ref_fName, floatNPoints=floatNPoints)

    gap_df = calculate_gap(dispersions, ref_dispersions, start_k, k_max)

    bestK = -1;
    for diffI in range(gap_df['diff'].shape[0]):
        if gap_df['diff'][diffI] > 0 and diffI+1 > 1:
            bestK = diffI+start_k;
            break;
    print("The optimal K is", bestK)
    # Find the optimal K
    fig, ax = plt.subplots()
    sumStr = ''
    plt.errorbar(range(start_k, k_max+1), gap_df['gap_value'], yerr=gap_df['sk'], fmt='-o')
    plt.ylabel('gap')
    plt.xlabel('k')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_gap{sumStr}.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_gap{sumStr}.pdf",bbox_inches='tight')
    fig, ax = plt.subplots()
    plt.plot(range(start_k, k_max+1), gap_df['diff'], '-o')
    plt.ylabel('gap$_{k}$-(gap$_{k+1}$+$\sigma$(gap$_{k+1}$))')
    plt.xlabel('k (number of clusters)')
    plt.xlim([start_k-1,k_max+1])
    ax.set_xticks([start_k+i*2 for i in range(int((k_max+1)/2))])
    ax.text(0.65, 0.2, f'Optimal $k$ ={bestK}', transform=ax.transAxes)#, fontsize=font)
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_diff{sumStr}.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_diff{sumStr}.pdf",bbox_inches='tight')

    # Repeat for gap*. The outcome is not expected to be consistent but it's interesting to see alternatives.
    fig, ax = plt.subplots()
    # Get the best k which is the first k for which gap(k)-(gap(k+1)-sigma(k+1))>=0
    bestKStar = np.argmax(gap_df['gap*'])+start_k
    print("Best KStar", bestKStar)
    plt.errorbar(range(start_k, k_max+1), gap_df['gap*'], fmt='-o')
    plt.ylabel('gap*')
    plt.xlabel('k (number of clusters)')
    plt.xlim([start_k-1,k_max+1])
    ax.set_xticks([start_k+i*2 for i in range(int((k_max+1)/2))])
    ax.text(0.25, 0.2, f'Optimal $k$={bestK} with diff method', transform=ax.transAxes)#, fontsize=font)
    ax.text(0.25, 0.3, f'Optimal $k$={bestKStar} with gap* method', transform=ax.transAxes)#, fontsize=font)
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_gapstar{sumStr}.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_gapstar{sumStr}.pdf",bbox_inches='tight')

    # Get the centroids and lables.
    bestNClusIndex = bestNClus-start_k
    centroids = kmeans_instances[bestNClusIndex].cluster_centers_
    clusLabels = labels.loc[:,bestNClusIndex].to_numpy()

    # Get cluster boundaries
    bins = {'MET':[i*50. for i in range(20)],
            'HT':[i*100. for i in range(20)],
            'LT':[i*10. for i in range(20)],
            'HTb':[i*20. for i in range(20)],
            'pT_1jet':[i*50. for i in range(20)],
            'pT_1bjet':[i*50. for i in range(20)],
            'pT_2jet':[i*20. for i in range(20)],
            'pT_3jet':[i*20. for i in range(10)],
            'pT_4jet':[i*20. for i in range(10)],
            'pT_1lep':[i*20. for i in range(20)],
            'nj_good':[i-0.5 for i in range(10)],
            'num_bjets':[i-0.5 for i in range(5)],
            'nLep_signal':[i-0.5 for i in range(5)],}
    
    
    rescaledCentroids = copy.deepcopy(centroids)
    for i, branch in enumerate(trainBranches):
        rescaledCentroids[:,i] = rescaledCentroids[:,i]*scales[branch]+mins[branch]
        
    sampNames = []
    sampNames_fName = f'{outDir}/{outPrefix}{excludedStr}{zeroStr}_sampNames.h5'
    sampTotalEvents_temp = {}
    if os.path.exists(sampNames_fName) and redo_cInd == False:
        print("Loading sample names from file ", sampNames_fName)
        allSampNameData = pandas.read_hdf(sampNames_fName, key='sampNames')
        allCrossSections = pandas.read_hdf(sampNames_fName, key='cross_section')
        sampTotalEvents = pandas.read_hdf(sampNames_fName, key='sampTotalEvents')
    else:
        print("Getting sample names")
        sampNameData = []
        crossSection = []
        for path in dataFilePaths:
            print("Processing: ", path)
            if removeZeros:
                if excluded == 0 or excluded == 1:
                    tempData = pandas.read_hdf(path, columns=trainBranches+["sampName", "excluded", "cross_section"])
                    tempData = tempData[tempData.excluded==excluded]
                else:
                    tempData = pandas.read_hdf(path, columns=trainBranches+["sampName", "cross_section"])
                tempSampNames = tempData['sampName'].unique()

                for tempSampName in tempSampNames:
                    if tempSampName not in sampTotalEvents_temp:
                        sampTotalEvents_temp[tempSampName] = tempData[tempData.sampName==tempSampName].shape[0]
                    else:
                        sampTotalEvents_temp[tempSampName] += tempData[tempData.sampName==tempSampName].shape[0]
                index = (tempData[trainBranches[0]] <= minTrainVal[trainBranches[0]])
                for branch in trainBranches[1:]:
                    index = (index) & (tempData[branch] <= minTrainVal[branch])
                tempData=tempData[~index]
            else:
                if excluded == 0 or excluded == 1:
                    tempData = pandas.read_hdf(path, columns=["sampName", "excluded", "cross_section"])
                    tempData = tempData[tempData.excluded==excluded]
                else:
                    tempData = pandas.read_hdf(path, columns=["sampName", "cross_section"])
                    
                tempSampNames = tempData['sampName'].unique()
                for tempSampName in tempSampNames:
                    if tempSampName not in sampTotalEvents_temp:
                        sampTotalEvents_temp[tempSampName] = tempData[tempData.sampName==tempSampName].shape[0]
                    else:
                        sampTotalEvents_temp[tempSampName] += tempData[tempData.sampName==tempSampName].shape[0]

                    
            tempData = tempData[tempData.cross_section>minCrossSec]
            if excluded == 1:
                tempData = tempData[tempData.cross_section<maxCrossSec]
                
            if tempData.empty:
                continue
            sampNameData.append(tempData['sampName'])
            crossSection.append(tempData['cross_section'])
        allSampNameData = pandas.concat(sampNameData)
        allCrossSections = pandas.concat(crossSection)
        sampTotalEvents = pandas.Series(sampTotalEvents_temp)
        allSampNameData.to_hdf(sampNames_fName, key='sampNames')
        allCrossSections.to_hdf(sampNames_fName, key='cross_section')
        sampTotalEvents.to_hdf(sampNames_fName, key='sampTotalEvents')

    sampNames.extend(allSampNameData.unique())
    print(f"We will process {len(sampNames)} models")
    shortNames = {}
    for sampLabel in sampNames:
        shortName = sampLabel.replace('pMSSM_MCMC', '').replace('___', '_')
        shortNames[sampLabel] = shortName

    bestClusters = {}
    bestClusters_yield = {}
    bestClusFracs = {}
    bestClusHist_yield = np.zeros((bestNClus))
    bestClusHist = np.zeros((bestNClus))
    clusFracs = {}
    bestClusYields = {}
    clusYields = {}

    clusIndeces_fName = f'{outDir}/{outPrefix}{excludedStr}{zeroStr}_cIndices_nClus{bestNClus}__{varListStr}.h5'
    if os.path.exists(clusIndeces_fName) and redo_cInd == False:
        print("Loading cluster indeces from file ", clusIndeces_fName)
        clusIndeces = pandas.read_hdf(clusIndeces_fName, key='clusIndeces')
    else:
        print("Getting clusters indeces")
        cInd_args = [[clusI, clusLabels] for clusI in range(bestNClus)]
        with multiprocessing.Pool(processes=bestNClus) as pool:
            cInd_results = pool.starmap(getClusIndeces, cInd_args)

        clusIndeces = pandas.DataFrame(columns=[i for i in range(bestNClus)])
        for cI, result in enumerate(cInd_results):
            clusIndeces[cI] = result
        clusIndeces.to_hdf(clusIndeces_fName, key='clusIndeces')

    # Clusters that contain the highest fraction of a signal are considered the best cluster.
    cFrac_fName = f'{outDir}/{outPrefix}{excludedStr}{zeroStr}_cFrac_nClus{bestNClus}__{varListStr}.pkl'
    if os.path.exists(cFrac_fName) and redo_cFracs == False:
        print("Loading cluster fractions from file.")
        with open(cFrac_fName, "rb") as cFrac_file:
            frac_results = pickle.load(cFrac_file)
    else:
        print("Getting clusters with the highest fraction of a particular signal")
        clusIndecesNP = clusIndeces.to_numpy()
        print(allSampNameData.shape, clusIndecesNP.shape)
        frac_args = [[sampName, allSampNameData, bestNClus, clusIndecesNP] for sampName in sampNames]
        with multiprocessing.Pool(processes=36) as pool:
            frac_results = pool.starmap(findClusFracs, frac_args)

        with open(cFrac_fName, 'wb') as handle:
            pickle.dump(frac_results, handle)
       
    print("Done getting fractions. Now reorganizing")
    for sampLabel, [bestClus, highestFrac, fracs] in zip(sampNames, frac_results): 
        bestClusters[shortNames[sampLabel]] = bestClus
        bestClusHist[bestClus]+=1
        bestClusFracs[shortNames[sampLabel]] = highestFrac
        clusFracs[shortNames[sampLabel]] = fracs


    # Clusters that contain the highest yield of a signal are considered the best cluster.
    cYield_fName = f'{outDir}/{outPrefix}{excludedStr}{zeroStr}_cYield_nClus{bestNClus}__{varListStr}.pkl'
    if os.path.exists(cYield_fName) and redo_cYields == False:
        print("Loading cluster yields from file.")
        with open(cYield_fName, "rb") as cYield_file:
            yield_results = pickle.load(cYield_file)
    else:
        print("Getting clusters with the highest yield of a particular signal")
        clusIndecesNP = clusIndeces.to_numpy()
        yield_results = []
        yield_args = [[sampName, allSampNameData, bestNClus, clusIndecesNP, allCrossSections, lumi, sampTotalEvents[sampName]] for sampName in sampNames]
        with multiprocessing.Pool(processes=36) as pool:
            yield_results = pool.starmap(findClusYields, yield_args)
        #for sampName in sampNames:
        #    yield_results.append(findClusYields(sampName, allSampNameData, bestNClus, clusIndecesNP, allCrossSections, lumi, sampTotalEvents))

        with open(cYield_fName, 'wb') as handle:
            pickle.dump(yield_results, handle)
       
    print("Done getting yield. Now reorganizing")
    for sampLabel, [bestClus, highestYield, yields] in zip(sampNames, yield_results): 
        bestClusters_yield[shortNames[sampLabel]] = bestClus
        bestClusHist_yield[bestClus]+=1
        bestClusYields[shortNames[sampLabel]] = highestYield
        clusYields[shortNames[sampLabel]] = yields    
    
    print("Plotting results")
    colors = ['r', 'g', 'b', 'y', 'c', 'k', 'm', 'tab:orange', 'tab:brown', 'olive', 'tab:purple', 'tab:pink', 'tab:blue', 'grey', 'pink', 'darkslateblue', 'powderblue', 'tomato', 'lime', 'slategray']

    fig, ax = plt.subplots()
    plotBestClusHist = np.append(np.insert(bestClusHist, 0, bestClusHist[0]), bestClusHist[-1])
    x = np.arange(-1, bestNClus+1)
    plt.step(x, plotBestClusHist/len(sampNames), where='mid')
    #plt.fill_between(np.arange(bestNClus)+0.5, bestClusHist, step='pre', alpha=0.3)
    ax.set_xlabel("Cluster")
    ax.set_ylabel("Fraction of models")
    plt.xlim([-0.5,bestNClus-0.5])
    plt.ylim([0,max(plotBestClusHist/len(sampNames))*1.2])
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_k{bestNClus}_bestClusOccupancy.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_k{bestNClus}_bestClusOccupancy.pdf",bbox_inches='tight')

    rng = np.random.default_rng()
    indeces = rng.choice(len(sampNames), bestNClus, replace=False)
    fig, ax = plt.subplots()
    maxFracs=[]
    for index in indeces:
        sampLabel = sampNames[index]
        plotClusFracs = np.append(np.insert(clusFracs[shortNames[sampLabel]], 0, clusFracs[shortNames[sampLabel]][0]), clusFracs[shortNames[sampLabel]][-1])

        plt.step(x, plotClusFracs, where='mid', label=shortNames[sampLabel])
        maxFracs.append(max(clusFracs[shortNames[sampLabel]]))

    ax.set_xlabel("Cluster")
    ax.set_ylabel("Fraction in cluster")
    plt.xlim([-0.5,bestNClus-0.5])
    plt.ylim([0,max(maxFracs)*2])
    if bestNClus < 5:
        ax.legend()
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_k{bestNClus}_clusOccupancy{sampLabel}.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_k{bestNClus}_clusOccupancy{sampLabel}.pdf",bbox_inches='tight')

    print("Plotting cluster and model kinematics")
    
    units = {branch: ' GeV' for branch in trainBranches+obsBranches}
    units['nj_good'] = ''
    units['nLep_signal'] = ''
    units['num_bjets'] = ''

    highestFracModels = []
    highestYieldModels = []
    maxFrac = -1
    fig, ax = plt.subplots()
    allSortedClus = []
    allSortedClus_yield = []
    
    for clusI in range(bestNClus):
        # Now let's figure out what models have the highest fraction of events in the interesting cluster.
        print("Sorting models according to the fraction of events they have in cluster", clusI)
        interestingClusFracs = {}
        interestingClusYields = {}
        for sampLabel in sorted(sampNames):
            interestingClusFracs[shortNames[sampLabel]] = clusFracs[shortNames[sampLabel]][clusI]
            interestingClusYields[shortNames[sampLabel]] = clusYields[shortNames[sampLabel]][clusI]
        sortedClus = {k: v for k, v in sorted(interestingClusFracs.items(), key=lambda item: item[1], reverse=True)}
        sortedClus_yield = {k: v for k, v in sorted(interestingClusYields.items(), key=lambda item: item[1], reverse=True)}
        allSortedClus.append(sortedClus)
        allSortedClus_yield.append(sortedClus_yield)
        fracs = list(sortedClus.values())
        plt.plot(np.arange(len(fracs)), fracs, marker='o', markersize=2, color=colors[clusI], label=f'Cluster {clusI}')
        if fracs[0] > maxFrac:
            maxFrac = fracs[0]

        topModels = ""
        topModels_yield = ""
        nTopModels = 3
        for modelName in list(sortedClus.keys())[:nTopModels]:
            topModels += f"{modelName:20}:{round(sortedClus[modelName]*100)}%\n"
        for modelName in list(sortedClus_yield.keys())[:nTopModels]:
            topModels_yield += f"{modelName:20}:{round(sortedClus_yield[modelName],1)}, {round(sortedClus[modelName]*100)}%, {allCrossSections[allSampNameData==modelName].iloc[0]} events\n"
        
        modelNames = sortedClus.keys()
        highestFracModel = list(modelNames)[0]
        highestFracModels.append(highestFracModel)

        modelNames_yield = sortedClus_yield.keys()
        highestYieldModel = list(modelNames_yield)[0]
        highestYieldModels.append(highestYieldModel)
        print(f"\nModels with the highest percentage of events in cluster {clusI}:\n{topModels}")
        print(f"\nModels with the highest event yield (fraction) in cluster {clusI}:\n{topModels_yield}")
        print("Values of centroids for cluster", clusI)
        print(', '.join([f"{trainBranches[i]}={round(rescaledCentroids[clusI][i]):.0f}{units[trainBranches[i]]}" for i in range(len(rescaledCentroids[0]))]))

    ax.set_xlabel("Model (sorted)")
    ax.set_ylabel(f"Fraction in cluster")
    if bestNClus < 5:
        ax.legend()
    plt.ylim([0, maxFrac*1.2])
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_clusFracs_k{bestNClus}{sumStr}.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_clusFracs_k{bestNClus}{sumStr}.pdf",bbox_inches='tight')
     
    refPoints = np.zeros(centroids.shape)
    errShape = [centroids.shape[i] for i in range(len(centroids.shape))]
    errShape.append(2)
    errors = np.zeros(errShape)
    print("")
    clusBounds = [{} for i in range(bestNClus)]
    for clusI in range(bestNClus):
        for branch in trainBranches+obsBranches:
            clusBounds[clusI][branch] = [np.nan, np.nan, np.nan]
            
    if plotErrors or plotDists:
        for varI, trainVar in enumerate(trainBranches+obsBranches):
                #print("Getting data for", trainVar)
                pdList = []
                for path in dataFilePaths:

                    if removeZeros:
                        if excluded == 0 or excluded == 1:
                            tempData = pandas.read_hdf(path, columns=trainBranches+["excluded", "cross_section"]+obsBranches)
                            tempData = tempData[tempData.excluded==excluded]
                            if excluded == 1:
                                tempData = tempData[tempData.cross_section<maxCrossSec]
                        else:
                            tempData = pandas.read_hdf(path, columns=trainBranches+["cross_section"]+obsBranches)
                            
                        tempData = tempData[tempData.cross_section>minCrossSec]
                        index = (tempData[trainBranches[0]] <= minTrainVal[trainBranches[0]])
                        for branch in trainBranches[1:]:
                            index = (index) & (tempData[branch] <= minTrainVal[branch])
                        tempData=tempData[~index]
                        tempData = tempData[trainVar]

                    else:
                        if excluded == 0 or excluded == 1:
                            tempData = pandas.read_hdf(path, columns=[trainVar, "excluded", "cross_section"])
                            tempData = tempData[tempData.excluded==excluded]
                            tempData = tempData[tempData.cross_section>minCrossSec]
                            if excluded == 1:
                                tempData = tempData[tempData.cross_section<maxCrossSec]
                            tempData = tempData[trainVar]
                        else:
                            tempData = pandas.read_hdf(path, columns=[trainVar]+["cross_section"])
                            tempData = tempData[tempData.cross_section>minCrossSec]
                            tempData = tempData[trainVar]
                    pdList.append(tempData)
                         
                varData = pandas.concat(pdList)
                
                #print("Rescaling data")
                rescaledData = copy.deepcopy(varData)
                rescaledData = rescaledData*scales[trainVar]+mins[trainVar]

                #print("Looping over clusters")
                for clusI in range(bestNClus):
                    clusIndex = clusIndeces[clusI].to_numpy()
                    if np.sum(clusIndex) == 0:
                        print("WARNING: You've got an empty cluster:", clusI)
                        continue
                    indivClusData = varData.loc[clusIndex].to_numpy()
                    ref = np.mean(indivClusData)
                    if trainVar in trainBranches:
                        refPoints[clusI,varI] = ref

                        if plotErrors:    
                            lowErr = np.std(indivClusData)
                            highErr = lowErr
                            errors[clusI,varI,0] = lowErr
                            errors[clusI,varI,1] = highErr
                        else:
                            errors[clusI, varI, 0] = 0
                            errors[clusI, varI, 1] = 0

                    if plotDists:
                        #print("Plotting distributions for cluster", clusI)
                        rescaledClusData = rescaledData[clusIndex]
                        weights = np.ones((rescaledClusData.shape[0]))
                        if weightStr != '':
                            weights = allCrossSections[clusIndex]
                            
                        fig, ax = plt.subplots()
                        if trainVar in bins:
                            (counts, npBins) = np.histogram(rescaledClusData, bins=bins[trainVar], weights=weights)
                            minVal= round(np.percentile(rescaledClusData, 10))
                            medVal= round(np.percentile(rescaledClusData, 50))
                            maxVal = round(np.percentile(rescaledClusData, 90))
                            clusBounds[clusI][trainVar] = [minVal, medVal, maxVal]
                            #print(f"Minimum, median, maximum for cluster {clusI} and {trainVar}: {minVal}, {medVal}, {maxVal}")
                            normFact = 0
                            if np.sum(weights) > 0:
                                normFact = 1./np.sum(weights)
                                
                            clusterHist = (counts, npBins, normFact)
                            plt.hist(npBins[:-1], npBins, weights=normFact*counts, histtype='step')
                        else:
                            print("Skipping", trainVar, "due to lack of binning definition")
                            continue
                        if units[trainVar] != '':
                            ax.set_xlabel(trainVar+' ['+units[trainVar].lstrip()+']')
                        else:
                            ax.set_xlabel(trainVar)
                        ax.set_ylabel(f"Fraction of events/{int(bins[trainVar][1]-bins[trainVar][0])}{units[trainVar]}")
                        # plt.savefig(f"{outDir}/plots/{outPrefix}_clus_{clusI}_{trainVar}_k{bestNClus}{sumStr}.svg",bbox_inches='tight')
                        # plt.savefig(f"{outDir}/plots/{outPrefix}_clus_{clusI}_{trainVar}_k{bestNClus}{sumStr}.pdf",bbox_inches='tight')

                        fig, ax = plt.subplots()
                        if trainVar in bins:
                            for modelName in list(allSortedClus_yield[clusI].keys())[:nTopModels]:
                                #tempFrac = allSortedClus[clusI][modelName]
                                tempYield = allSortedClus_yield[clusI][modelName]
                                sampIndex = (allSampNameData==modelName)
                                rescaledModelData = rescaledData[sampIndex]
                            
                                (counts, npBins) = np.histogram(rescaledModelData, bins=bins[trainVar])
                                normFact = 0
                                if rescaledModelData.shape[0] > 0:
                                    normFact = 1./rescaledModelData.shape[0]
                            
                                #plt.hist(npBins[:-1], npBins, weights=normFact*counts, histtype='step', linestyle='dashed', label=f"Model {modelName.split('_')[-1]} ({round(tempFrac*100)}%)")
                                plt.hist(npBins[:-1], npBins, weights=normFact*counts, histtype='step', linestyle='dashed', label=f"Model {modelName.split('_')[-1]} ({round(tempYield,1)})")
                            plt.hist(npBins[:-1], npBins, weights=clusterHist[0]*clusterHist[2], histtype='step', label=f'Cluster {clusI}')
                        else:
                            print("Skipping", trainVar, "due to missing binning")

                        if units[trainVar] != '':
                            ax.set_xlabel(trainVar+' ['+units[trainVar]+']')
                        else:
                            ax.set_xlabel(trainVar)
                        ax.set_ylabel(f"Fraction of events/{int(bins[trainVar][1]-bins[trainVar][0])} {units[trainVar]}")
                        ax.legend()
                        plt.ylim([0, ax.get_ylim()[1]*1.3])
                        plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_clus_{clusI}_{trainVar}_k{bestNClus}{sumStr}.svg",bbox_inches='tight')
                        plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_clus_{clusI}_{trainVar}_k{bestNClus}{sumStr}.pdf",bbox_inches='tight')

                del varData
                gc.collect()
                plt.close('all')
        errors = np.swapaxes(errors, 1 ,2)
    else:
        refPoints = centroids
        errors = np.zeros(centroids.shape)

    #print("Dumping cluster boundaries:")
    tableF = open(f'{outDir}/plots/lower_bounds_{outPrefix}{excludedStr}{zeroStr}_k{bestNClus}{sumStr}.tex', 'w')
    standalone_tables = True
    if standalone_tables:
        tableF.write("\\documentclass{standalone}\n"
                     "\\begin{document}\n")
    tableF.write("\\begin{tabular}{l||")
    tableF.write("c|"*len(trainBranches+obsBranches))
    tableF.write("}\n\\hline\\hline\n")
    tableF.write("& "+" & ".join([branch.replace('_', '\\_') for branch in trainBranches+obsBranches])+"\\\\ \\hline\\hline")
        
    for clusI, bounds in enumerate(clusBounds):
        #print(f"Cluster {clusI}:")
        tableF.write(f"\nCluster {clusI} ")
        
        for branch in bounds:
            if branch not in trainBranches+obsBranches:
                continue
            # For the clean table let's ignore the upper boundary
            if bounds[branch][0] < 0:
                boundStr = '-'
            else:
                boundStr = f"{bounds[branch][0]}"

            tableF.write(f"& {boundStr}")
            if bounds[branch][0] < 0 and bounds[branch][2] < 0:
                continue
            #print(f"{branch:15} & {bounds[branch][0]:10} & {bounds[branch][1]:10}\\\\ \\hline")
            
        tableF.write("\\\\ \\hline")
    tableF.write("\n\\end{tabular}\n")
    if standalone_tables:
        tableF.write("\\end{document}\n")
    tableF.close()
    
    fig, ax = plt.subplots()
    bestClusIndex = sorted(list(set(bestClusters.values())))
    bestClusLabels=[f'Cluster {clusI}' for clusI in bestClusIndex]
    maxY = 0
    for cI in range(bestNClus):
        maxRef = max(refPoints[cI,:])
        if maxY < maxRef:
            maxY = maxRef
        plt.errorbar(np.arange(len(trainBranches)), refPoints[cI,:],
                     yerr=errors[cI,:], label=f'Cluster {cI}', marker='o', color=colors[cI], markerfacecolor='none')

    ax.set_xlabel("Observable")
    ax.set_ylabel("Cluster centroid")
    ax.set_xticks([i for i in range(len(trainBranches))])
    ax.set_xticklabels(trainBranches, rotation=90)
    #plt.ylim([-0.1, maxY+(maxY+0.1)*.6])
    plt.ylim([0, maxY+(maxY+0.1)*.6])
    plt.xlim([-0.1, len(trainBranches)+.3-1])
    if bestNClus < 5:
        ax.legend(ncol=2)
    plt.plot([-0.1, len(trainBranches)+.2], [0, 0], 'k-', lw=2)
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_obs_k{bestNClus}{sumStr}.svg",bbox_inches='tight')
    plt.savefig(f"{outDir}/plots/{outPrefix}{excludedStr}{zeroStr}_obs_k{bestNClus}{sumStr}.pdf",bbox_inches='tight')
    plt.close('all')
    
    del allSampNameData
    gc.collect()
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-p', '--path', default='/users/whopkins/pMSSM_clustering/sigclustering/sigclustering/python/pMSSM_EW_nearContour*.h5')
    parser.add_argument('--minMax_fName', default='minsAndScales_EW_nearContour.pkl')
    parser.add_argument('--outPrefix', default='EW_nearContour')
    parser.add_argument('--outDir', default='EW_nearContour/')
    parser.add_argument('--weight', default='')
    parser.add_argument('--startFile', type=int, default=0)
    parser.add_argument('--endFile', type=int, default=-1)
    parser.add_argument('--bestNClus', type=int, default=3)
    parser.add_argument('--floatNPoints', type=float, default=10)
    parser.add_argument('--minCrossSec', type=float, default=.005, help="Minimum cross section that models are allowed to have in pb")
    parser.add_argument('--maxCrossSec', type=float, default=.03, help="Maximum cross section that models are allowed to have in pb")
    parser.add_argument('--redo_kmeans', action='store_true')
    parser.add_argument('--redo_labels', action='store_true')
    parser.add_argument('--redo_refs', action='store_true')
    parser.add_argument('--redo_cFracs', action='store_true')
    parser.add_argument('--redo_cYields', action='store_true')
    parser.add_argument('--redo_cInd', action='store_true')
    parser.add_argument('--plot_errors', action='store_true')
    parser.add_argument('--plot_dists', action='store_true')
    parser.add_argument('--excluded', type=int, default=-1)
    parser.add_argument('--k_max', type=int, default=35)
    parser.add_argument('--removeZeros', action='store_true')

    args = parser.parse_args()

    cluster(args.path, 
            args.redo_kmeans, args.redo_labels, args.redo_refs, args.redo_cFracs, args.redo_cYields, args.redo_cInd,
            args.minMax_fName, endFile=args.endFile, bestNClus=args.bestNClus,
            plotErrors=args.plot_errors, plotDists=args.plot_dists,
            outPrefix=args.outPrefix, outDir=args.outDir,
            weightStr=args.weight,
            excluded=args.excluded, removeZeros=args.removeZeros,
            k_max=args.k_max,
            floatNPoints=args.floatNPoints, minCrossSec=args.minCrossSec, maxCrossSec=args.maxCrossSec)

