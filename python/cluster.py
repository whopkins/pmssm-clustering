#!/usr/bin/env python
import argparse, uproot, matplotlib, pandas
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder, MinMaxScaler
import numpy as np

matplotlib.use('Agg')
import matplotlib.pyplot as plt
font = {'size':16}
matplotlib.rc('font', **font)

def cluster(inPath1, inPath2):
    tree1 = uproot.open(inPath1)['Nominal']
    dataset1 = tree1.arrays(['*'], outputtype=pandas.DataFrame)

    tree2 = uproot.open(inPath2)['Nominal']
    dataset2 = tree2.arrays(['*'], outputtype=pandas.DataFrame)
    
    fig, ax = plt.subplots()
    ptBins = [10+i*10 for i in range(20)]
    plt.hist(dataset1['pT_1lep'], bins=ptBins, histtype='step', density=True)
    plt.hist(dataset2['pT_1lep'], bins=ptBins, histtype='step', density=True)
    plt.savefig('pT_1lep.pdf', bbox_inches='tight')
    print(dataset1.keys())
    # # Dummy code. 
    # kmeans = KMeans(n_clusters=2, random_state=1984)
    # y_kmeans = kmeans.fit_predict(dataset)
    # #print(list(y_kmeans))
    # plt.clf()
    # print(len(kmeans.cluster_centers_[1]))
    # plt.scatter(kmeans.cluster_centers_[0], kmeans.cluster_centers_[0])
    # plt.savefig('clusters.pdf', bbox_inches='tight')

    x = -2 * np.random.rand(200,2)
    x0 = 1 + 2 * np.random.rand(100,2)
    x[100:200, :] = x0
    plt.clf()
    plt.scatter(x[ : , 0], x[ :, 1], s = 25, color='r')
    plt.savefig('inputScatter.pdf', bbox_inches='tight')

    Kmean = KMeans(n_clusters=2)
    Kmean.fit(x)
    centroids = Kmean.cluster_centers_
    plt.scatter(centroids[:,0], centroids[:,1], s=100)
    plt.savefig('centroids.pdf', bbox_inches='tight')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Cluster something')
    parser.add_argument('--inPath1', type=str, default='/users/ekourlitis/atlasfs02/ChargedHiggs/AnaSkim/2020-06-05/cHiggs600_x1n2h_mC1_300_mN1_150.root')
    parser.add_argument('--inPath2', type=str, default='/users/aparamonov/work/cH_AnaSkim/DAOD_SUSY1.cHiggs600_x1n2Z_mC1_300_mN1_150.root')

    args = parser.parse_args()

    cluster(args.inPath1, args.inPath2)

