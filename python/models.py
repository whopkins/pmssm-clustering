from typing import Tuple
from collections import OrderedDict
import torch
randomSeed = 5
torch.manual_seed(randomSeed)
import random
random.seed(randomSeed)
import numpy as np
np.random.seed(randomSeed)
import torch.nn as nn
from torch.autograd import Function

from joblib import Parallel, delayed

def weighted_mse_loss(inputs: torch.Tensor, targets: torch.Tensor, weights: torch.Tensor):
    perSampleMSE = ((inputs - targets)*(inputs - targets)).mean(dim=1)
    print("Inputs", inputs)
    print("Targets", targets)
    print("Weights are", weights)
    print("Per sample MSE is", perSampleMSE)
    #loss = (weights*perSampleMSE).mean()
    loss = (perSampleMSE).mean()
    print("Loss", loss)
    return loss

class autoencoder_old(nn.Module):
    def __init__(self, num_features: int, latent_dim: int) -> None:
        super().__init__()
        self.encoder = nn.Sequential(
            nn.Linear(num_features, latent_dim),
            #nn.Sigmoid()
            nn.ReLU()
        )
        self.decoder = nn.Sequential(
            nn.Linear(latent_dim, num_features),
            #nn.Sigmoid()
        )

        #self.test = nn.Linear(num_features, latent_dim)
        self.criterion = nn.MSELoss()
        #self.criterion = weighted_mse_loss

    # Targets are not used but Samba needs it.
    #def forward(self, inputs: torch.Tensor, targets: torch.Tensor, weights: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
    def forward(self, inputs: torch.Tensor, targets: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        x = self.encoder(inputs)
        out = self.decoder(x)
        #loss = self.criterion(out, targets, weights)
        loss = self.criterion(out, targets)
        return loss, out

    
class autoencoder(nn.Module):
    def __init__(self, num_features: int, latent_dim: int, reduction:int=2, sigmoidEncoderOut:bool=False, sigmoidDecoderOut:bool=False) -> None:
        super().__init__()
        # Figure out how many hidden dimensions we can have if we reduce the dimensionality by two for each layer
        hiddenLayerInfo = []
        nNodes = num_features
        while nNodes > latent_dim+reduction:
            inputNodes = nNodes
            nNodes -= reduction
            hiddenLayerInfo.append((inputNodes, nNodes))
        hiddenLayerInfo.append((nNodes, latent_dim))
        hiddenEncoderLayers = []
        hiddenDecoderLayers = []
        for inputNodes, outputNodes in hiddenLayerInfo:
            hiddenEncoderLayers.append(nn.Linear(inputNodes, outputNodes))
            hiddenEncoderLayers.append(nn.ReLU())

        # WH: No ReLU on the output layer
        hiddenEncoderLayers.pop()

        # WH: Might be useful if you want your latent space to be with [0,1]
        if sigmoidEncoderOut:
            hiddenEncoderLayers.append(nn.Sigmoid())
            
        for outputNodes, inputNodes in reversed(hiddenLayerInfo):
            hiddenDecoderLayers.append(nn.Linear(inputNodes, outputNodes))
            hiddenDecoderLayers.append(nn.ReLU())

        # WH: No ReLU on the output layer
        hiddenDecoderLayers.pop()

        # WH: Might be useful if your output is scaled from 0-1.
        if sigmoidDecoderOut:
            hiddenDecoderLayers.append(nn.Sigmoid())
               
        self.encoder = nn.Sequential(
            *hiddenEncoderLayers,
        )
        self.decoder = nn.Sequential(
            *hiddenDecoderLayers,
        )

        self.criterion = nn.MSELoss(reduction='sum')
        
    # Targets are not used but Samba needs it.
    def forward(self, inputs: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        x = self.encoder(inputs)
        out = self.decoder(x)
        loss = self.criterion(out, inputs)
        return loss, out

class autoencoder_KL(nn.Module):
    def __init__(self, num_features: int, latent_dim: int, reduction:int=2,
                 sigmoidEncoderOut:bool=False, sigmoidDecoderOut:bool=False,
                 nBins=20) -> None:
        super().__init__()

        self.nBins = nBins
        # Figure out how many hidden dimensions we can have if we reduce the dimensionality by two for each layer
        hiddenLayerInfo = []
        nNodes = num_features
        while nNodes > latent_dim+reduction:
            inputNodes = nNodes
            nNodes -= reduction
            hiddenLayerInfo.append((inputNodes, nNodes))
        hiddenLayerInfo.append((nNodes, latent_dim))
        hiddenEncoderLayers = []
        hiddenDecoderLayers = []
        for inputNodes, outputNodes in hiddenLayerInfo:
            hiddenEncoderLayers.append(nn.Linear(inputNodes, outputNodes))
            hiddenEncoderLayers.append(nn.ReLU())

        # WH: No ReLU on the output layer
        hiddenEncoderLayers.pop()

        # WH: Might be useful if you want your latent space to be with [0,1]
        if sigmoidEncoderOut:
            hiddenEncoderLayers.append(nn.Sigmoid())
            
        for outputNodes, inputNodes in reversed(hiddenLayerInfo):
            hiddenDecoderLayers.append(nn.Linear(inputNodes, outputNodes))
            hiddenDecoderLayers.append(nn.ReLU())

        # WH: No ReLU on the output layer
        hiddenDecoderLayers.pop()

        # WH: Might be useful if your output is scaled from 0-1.
        if sigmoidDecoderOut:
            hiddenDecoderLayers.append(nn.Sigmoid())
               
        self.encoder = nn.Sequential(
            *hiddenEncoderLayers,
        )
        self.decoder = nn.Sequential(
            *hiddenDecoderLayers,
        )

    criterion = torch.nn.KLDivLoss()
    
    # def loss(self, inputs, targets):
    #     batch_size = inputs.shape[0]
    #     inputsHistTensor = torch.ones(inputs.shape[1], self.nBins)
    #     for dim in range(inputs.shape[1]):
    #         inputsHistTensor[dim] = torch.histc(inputs[:,dim], bins=self.nBins)/batch_size

    #     targetsHistTensor = torch.ones(targets.shape[1], self.nBins)
    #     for dim in range(targets.shape[1]):
    #         targetsHistTensor[dim] = torch.histc(targets[:,dim], bins=self.nBins)/batch_size
    #     return self.criterion(inputsHistTensor, targetsHistTensor)
        
    # Targets are not used but Samba needs it.
    def forward(self, inputs: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        x = self.encoder(inputs)
        out = self.decoder(x)
        loss = self.criterion(out, inputs)
        return loss, out
    
# This based off of code from https://github.com/xuyxu/Deep-Clustering-Network
def _parallel_compute_distance(X, cluster):
    n_samples = X.shape[0]
    dis_mat = np.zeros((n_samples, 1))
    for i in range(n_samples):
        dis_mat[i] += np.sqrt(np.sum((X[i] - cluster) ** 2, axis=0))
    return dis_mat


class batch_KMeans(object):

    def __init__(self, n_features:int, n_clusters:int, n_jobs:int) -> None:
        self.n_features = n_features
        self.n_clusters = n_clusters
        self.clusters = torch.zeros(self.n_clusters, self.n_features)
        self.count = 100 * torch.ones(self.n_clusters)  # serve as learning rate
        self.n_jobs = n_jobs

    def _compute_dist(self, X):
        dis_mat = Parallel(n_jobs=self.n_jobs)(
            delayed(_parallel_compute_distance)(X, self.clusters[i])
            for i in range(self.n_clusters))
        dis_mat = np.hstack(dis_mat)

        return dis_mat

    def init_cluster(self, X, indices=None) -> None:
        """ Generate initial clusters using sklearn.Kmeans """
        model = KMeans(n_clusters=self.n_clusters,
                       n_init=20)
        model.fit(X)
        self.clusters = model.cluster_centers_  # copy clusters

    def update_cluster(self, X, cluster_idx):
        """ Update clusters in Kmeans on a batch of data """
        n_samples = X.shape[0]
        for i in range(n_samples):
            self.count[cluster_idx] += 1
            eta = 1.0 / self.count[cluster_idx]
            updated_cluster = ((1 - eta) * self.clusters[cluster_idx] +
                               eta * X[i])
            self.clusters[cluster_idx] = updated_cluster

    def update_assign(self, X):
        """ Assign samples in `X` to clusters """
        dis_mat = self._compute_dist(X)

        return np.argmin(dis_mat, axis=1)

class DCN_Autoencoder(nn.Module):

    def __init__(self, input_dim: int, hidden_dims, latent_dim: int, n_clusters: int, activation:str) -> None:
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = self.input_dim
        self.hidden_dims = hidden_dims
        self.hidden_dims.append(latent_dim)
        self.dims_list = (hidden_dims +
                          hidden_dims[:-1][::-1])  # mirrored structure
        self.n_layers = len(self.dims_list)
        self.latent_dim = latent_dim
        self.n_clusters = n_clusters

        # WH: This should be hard-coded for SambaNova. I think.
        self.activation = activation

        # # Validation check
        # assert self.n_layers % 2 > 0
        # assert self.dims_list[self.n_layers // 2] == self.latent_dim

        # Encoder Network
        layers = OrderedDict()
        for idx, hidden_dim in enumerate(self.hidden_dims):
            if idx == 0:
                layers.update(
                    {
                        'linear0': nn.Linear(self.input_dim, hidden_dim),
                        'activation0': getattr(nn,self.activation)()
                    }
                )
            else:
                layers.update(
                    {
                        'linear{}'.format(idx): nn.Linear(
                            self.hidden_dims[idx-1], hidden_dim),
                        'activation{}'.format(idx): getattr(nn,self.activation)(),
                        # 'bn{}'.format(idx): nn.BatchNorm1d(
                        #     self.hidden_dims[idx])
                    }
                )
        self.encoder = nn.Sequential(layers)

        #print('encoder: ',self.encoder)

        # Decoder Network
        layers = OrderedDict()
        tmp_hidden_dims = self.hidden_dims[::-1]
        for idx, hidden_dim in enumerate(tmp_hidden_dims):
            if idx == len(tmp_hidden_dims) - 1:
                layers.update(
                    {
                        'linear{}'.format(idx): nn.Linear(
                            hidden_dim, self.output_dim),
                        # 'activation{}'.format(idx): nn.LeakyReLU(),
                    }
                )
            else:
                layers.update(
                    {
                        'linear{}'.format(idx): nn.Linear(
                            hidden_dim, tmp_hidden_dims[idx+1]),
                        'activation{}'.format(idx): getattr(nn,self.activation)(),
                        # 'bn{}'.format(idx): nn.BatchNorm1d(
                        #     tmp_hidden_dims[idx+1])
                    }
                )
        self.decoder = nn.Sequential(layers)
        #print('decoder: ',self.decoder)
        
    def __repr__(self):
        repr_str = '[Structure]: {}-'.format(self.input_dim)
        for idx, dim in enumerate(self.dims_list):
            repr_str += '{}-'.format(dim)
        repr_str += str(self.output_dim) + '\n'
        repr_str += '[n_layers]: {}'.format(self.n_layers) + '\n'
        repr_str += '[n_clusters]: {}'.format(self.n_clusters) + '\n'
        repr_str += '[input_dims]: {}'.format(self.input_dim) + '\n'
        repr_str += '[activation]: {}'.format(self.activation)
        return repr_str

    def __str__(self):
        return self.__repr__()
    
    def forward(self, inputs:torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        latent = self.encoder(inputs)
        rec = self.decoder(latent)
        return latent, rec

    

# This based off of code from https://github.com/xuyxu/Deep-Clustering-Network
class DCN_new(nn.Module):

    def __init__(self, input_dim: int, hidden_dims, latent_dim: int, n_clusters: int, activation:str, beta:float, lamda:float, n_jobs:int) -> None:
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = self.input_dim
        self.hidden_dims = hidden_dims
        self.hidden_dims.append(latent_dim)
        self.dims_list = (hidden_dims +
                          hidden_dims[:-1][::-1])  # mirrored structure
        self.n_layers = len(self.dims_list)
        self.latent_dim = latent_dim
        self.n_clusters = n_clusters
        self.beta=beta
        self.lamda = lamda

        self.activation = activation

        # # Validation check
        # assert self.n_layers % 2 > 0
        # assert self.dims_list[self.n_layers // 2] == self.latent_dim

        # Encoder Network
        layers = OrderedDict()
        for idx, hidden_dim in enumerate(self.hidden_dims):
            if idx == 0:
                layers.update(
                    {
                        'linear0': nn.Linear(self.input_dim, hidden_dim),
                        'activation0': getattr(nn,self.activation)()
                    }
                )
            else:
                layers.update(
                    {
                        'linear{}'.format(idx): nn.Linear(
                            self.hidden_dims[idx-1], hidden_dim),
                        'activation{}'.format(idx): getattr(nn,self.activation)(),
                        # 'bn{}'.format(idx): nn.BatchNorm1d(
                        #     self.hidden_dims[idx])
                    }
                )
        self.encoder = nn.Sequential(layers)

        #print('encoder: ',self.encoder)

        # Decoder Network
        layers = OrderedDict()
        tmp_hidden_dims = self.hidden_dims[::-1]
        for idx, hidden_dim in enumerate(tmp_hidden_dims):
            if idx == len(tmp_hidden_dims) - 1:
                layers.update(
                    {
                        'linear{}'.format(idx): nn.Linear(
                            hidden_dim, self.output_dim),
                        # 'activation{}'.format(idx): nn.LeakyReLU(),
                    }
                )
            else:
                layers.update(
                    {
                        'linear{}'.format(idx): nn.Linear(
                            hidden_dim, tmp_hidden_dims[idx+1]),
                        'activation{}'.format(idx): getattr(nn,self.activation)(),
                        # 'bn{}'.format(idx): nn.BatchNorm1d(
                        #     tmp_hidden_dims[idx+1])
                    }
                )
        self.decoder = nn.Sequential(layers)
        #print('decoder: ',self.decoder)
        self.mseLoss = nn.MSELoss()
        self.kmeans = batch_KMeans(latent_dim, n_clusters, n_jobs)

    def __repr__(self):
        repr_str = '[Structure]: {}-'.format(self.input_dim)
        for idx, dim in enumerate(self.dims_list):
            repr_str += '{}-'.format(dim)
        repr_str += str(self.output_dim) + '\n'
        repr_str += '[n_layers]: {}'.format(self.n_layers) + '\n'
        repr_str += '[n_clusters]: {}'.format(self.n_clusters) + '\n'
        repr_str += '[input_dims]: {}'.format(self.input_dim) + '\n'
        repr_str += '[activation]: {}'.format(self.activation)
        return repr_str

    def __str__(self):
        return self.__repr__()

    def criterion(self, inputs:torch.Tensor, latent:torch.Tensor, rec:torch.Tensor):
        #batch_size = latent.size()[0]

        # Reconstruction error
        #rec_loss = self.lamda * self.mseLoss(inputs, rec)
        rec_loss = self.mseLoss(inputs, rec)
        # dist_loss = torch.Tensor([0])
        # total_loss = rec_loss+dist_loss
        return rec_loss#total_loss, rec_loss, dist_loss
        # Regularization term on clustering
        # dist_loss = torch.tensor(0.)#.to(self.device)
        # clusters = torch.FloatTensor(self.kmeans.clusters)#.to(self.device)
        # for i in range(batch_size):
        #     # print(f'dist_loss: {i} latent: {latent_X[i]} clusters: {clusters[cluster_id[i]]}')
        #     diff_vec = latent_X[i] - clusters[cluster_id[i]]
        #     # print(f'diff_vec: {diff_vec}')
        #     sample_dist_loss = torch.matmul(diff_vec.view(1, -1),
        #                                     diff_vec.view(-1, 1))
        #     # print(f'diff_vec: {sample_dist_loss}')
        #     dist_loss += 0.5 * self.beta * torch.squeeze(sample_dist_loss)
        #     # print(f'dist_loss: {dist_loss}')

        # return (rec_loss + dist_loss,
        #         #rec_loss.detach().cpu().numpy(),
        #         #dist_loss.detach().cpu().numpy())
        #         rec_loss,
        #         dist_loss)

    def forward(self, inputs:torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        batch_size = inputs.shape[0]
        latent = self.encoder(inputs)
        rec = self.decoder(latent)
        rec_loss = self.mseLoss(inputs, rec)
        dist_loss = torch.tensor(0.)#.to(self.device)
        clusters = torch.FloatTensor(self.kmeans.clusters)#.to(self.device)
        for i in range(batch_size):
            # print(f'dist_loss: {i} latent: {latent_X[i]} clusters: {clusters[cluster_id[i]]}')
            diff_vec = latent[i] - clusters[cluster_id[i]]
            # print(f'diff_vec: {diff_vec}')
            sample_dist_loss = torch.matmul(diff_vec.view(1, -1),
                                            diff_vec.view(-1, 1))
            # print(f'diff_vec: {sample_dist_loss}')
            dist_loss += 0.5 * self.beta * torch.squeeze(sample_dist_loss)
            # print(f'dist_loss: {dist_loss}')
        return rec_loss, rec

# This based off of code from https://github.com/xuyxu/Deep-Clustering-Network
class DCN(nn.Module):

    def __init__(self, input_dim: int, hidden_dims, latent_dim: int, n_clusters: int, activation:str,
                 beta:float, lamda:float) -> None:
        super().__init__()
        self.beta = beta  # coefficient of the clustering term
        self.lamda = lamda  # coefficient of the reconstruction term
        #self.device = torch.device('cuda' if args.cuda else 'cpu')
        
        # Validation check
        # if not self.beta > 0:
        #     msg = 'beta should be greater than 0 but got value = {}.'
        #     raise ValueError(msg.format(self.beta))

        # if not self.lamda > 0:
        #     msg = 'lamda should be greater than 0 but got value = {}.'
        #     raise ValueError(msg.format(self.lamda))

        # if len(hidden_dims) == 0:
        #     raise ValueError('No hidden layer specified.')

        #self.kmeans = batch_KMeans(latent_dim, n_clusters, n_jobs)
        self.autoencoder = DCN_Autoencoder(input_dim, hidden_dims, latent_dim, n_clusters, activation)#.to(self.device)

        # print('autoencoder structure:')
        # print(self.autoencoder)

        self.mseLoss = nn.MSELoss()
        
        # self.optimizer = torch.optim.Adam(self.parameters(),
        #                                   lr=lr,
        #                                   weight_decay=wd)
    def forward(self, inputs:torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        latent, rec  = self.autoencoder(inputs)
        #total_loss, rec_loss, dist_loss = self.criterion(inputs, latent, rec)#, cluster_id)
        #total_loss = self.criterion(inputs, latent, rec)#, cluster_id)
        total_loss = self.mseLoss(inputs,rec)
        return total_loss, rec
    """ Compute the Equation (5) in the original paper on a data batch """
    # def criterion(self, inputs:torch.Tensor, latent:torch.Tensor, rec:torch.Tensor) -> torch.Tensor:
    #     #batch_size = latent.size()[0]

    #     # Reconstruction error
    #     #rec_loss = self.lamda * self.mseLoss(inputs, rec)
    #     rec_loss = self.mseLoss(inputs, rec)
    #     # dist_loss = torch.Tensor([0])
    #     # total_loss = rec_loss+dist_loss
    #     return rec_loss#total_loss, rec_loss, dist_loss
    #     # Regularization term on clustering
    #     # dist_loss = torch.tensor(0.)#.to(self.device)
    #     # clusters = torch.FloatTensor(self.kmeans.clusters)#.to(self.device)
    #     # for i in range(batch_size):
    #     #     # print(f'dist_loss: {i} latent: {latent_X[i]} clusters: {clusters[cluster_id[i]]}')
    #     #     diff_vec = latent_X[i] - clusters[cluster_id[i]]
    #     #     # print(f'diff_vec: {diff_vec}')
    #     #     sample_dist_loss = torch.matmul(diff_vec.view(1, -1),
    #     #                                     diff_vec.view(-1, 1))
    #     #     # print(f'diff_vec: {sample_dist_loss}')
    #     #     dist_loss += 0.5 * self.beta * torch.squeeze(sample_dist_loss)
    #     #     # print(f'dist_loss: {dist_loss}')

    #     # return (rec_loss + dist_loss,
    #     #         #rec_loss.detach().cpu().numpy(),
    #     #         #dist_loss.detach().cpu().numpy())
    #     #         rec_loss,
    #     #         dist_loss)

    # # def pretrain(self, train_loader, epoch=100, verbose=True):

    # #     if not self.pretrain:
    # #         return

    # #     if not isinstance(epoch, numbers.Integral):
    # #         msg = '`epoch` should be an integer but got value = {}'
    # #         raise ValueError(msg.format(epoch))

    # #     if verbose:
    # #         print('========== Start pretraining ==========')

    # #     rec_loss_list = []

    # #     self.train()
    # #     for e in range(epoch):
    # #         for batch_idx, (data, _, _) in enumerate(train_loader):
    # #             batch_size = data.shape[0]
    # #             data = data.to(self.device).view(batch_size, -1)
    # #             rec_X = self.autoencoder(data)
    # #             loss = self.criterion(data, rec_X)

    # #             if verbose and batch_idx % self.args.log_interval == 0:
    # #                 msg = 'Epoch: {:02d} | Batch: {:03d} | Rec-Loss: {:.5f}'
    # #                 print(msg.format(e, batch_idx,
    # #                                  loss.detach().cpu().numpy()))
    # #                 rec_loss_list.append(loss.detach().cpu().numpy())

    # #             self.optimizer.zero_grad()
    # #             loss.backward()
    # #             self.optimizer.step()
    # #     self.eval()

    # #     if verbose:
    # #         print('========== End pretraining ==========\n')

    # #     # Initialize clusters in self.kmeans after pre-training
    # #     batch_X = []
    # #     targets = []
    # #     for batch_idx, (data, target, _) in enumerate(train_loader):
    # #         batch_size = data.size()[0]
    # #         data = data.to(self.device).view(batch_size, -1)
    # #         latent_X = self.autoencoder(data, latent=True)
    # #         batch_X.append(latent_X.detach().cpu().numpy())
    # #         targets.append(target.detach().cpu().numpy())
    # #     batch_X = np.vstack(batch_X)
    # #     targets = np.vstack(targets)


    # #     # plt.scatter(batch_X[:,0],batch_X[:,1],c=targets)
    # #     # plt.savefig('pretraining_latent.png')
    # #     # plt.xlim(batch_X[:,0].min(),batch_X[:,0].max())
    # #     # plt.ylim(batch_X[:,1].min(),batch_X[:,1].max())

    # #     self.kmeans.init_cluster(batch_X)

    # #     return rec_loss_list
    # # The target is a dummy in this case
    # def forward(self, inputs:torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
    #     latent, rec  = self.autoencoder(inputs, targets)
    #     #total_loss, rec_loss, dist_loss = self.criterion(inputs, latent, rec)#, cluster_id)
    #     #total_loss = self.criterion(inputs, latent, rec)#, cluster_id)
    #     total_loss = self.mseLoss(inputs,rec)
    #     return total_loss, rec
        # for batch_idx, (data, targets, weights) in enumerate(train_loader):
        #     batch_size = data.size()[0]
        #     data = data.view(batch_size, -1).to(self.device)

        #     # Get the latent features
        #     with torch.no_grad():
        #         latent_X = self.autoencoder(data, latent=True)
        #         latent_X = latent_X.cpu().numpy()

        #     # [Step-1] Update the assignment results
        #     cluster_id = self.kmeans.update_assign(latent_X)

        #     # [Step-2] Update clusters in bath Kmeans
        #     elem_count = np.bincount(cluster_id,
        #                              minlength=self.args.n_clusters)
        #     for k in range(self.args.n_clusters):
        #         # avoid empty slicing
        #         if elem_count[k] == 0:
        #             continue
        #         self.kmeans.update_cluster(latent_X[cluster_id == k], k)

        #     # [Step-3] Update the network parameters
        #     loss, rec_loss, dist_loss = self._loss(data, cluster_id)
        #     self.optimizer.zero_grad()
        #     loss.backward()
        #     self.optimizer.step() 

            
            # if verbose and batch_idx % self.args.log_interval == 0:
            #     accuracy = (targets - cluster_id).numpy().mean()
            #     msg = 'Train Epoch: {:02d} | Batch: {:03d} | Loss: {:.5f} | Rec-' \
            #           'Loss: {:.5f} | Dist-Loss: {:.5f} | Accuracy {:.5f}'
            #     print(msg.format(epoch, batch_idx,
            #                      loss.detach().cpu().numpy(),
            #                      rec_loss, dist_loss, accuracy))
            #     print(f'clusters: {self.kmeans.clusters}')
        #, rec_loss, dist_loss
