#!/usr/bin/env python
import glob, matplotlib, pandas, argparse, sys, pickle, multiprocessing, os, psutil, gc, copy, pathlib, csv, subprocess
randomState = 5
import numpy as np
np.random.seed(randomState)
from sklearn.cluster import KMeans, MiniBatchKMeans
import matplotlib.pyplot as plt
plt.style.use('default')
font = {'size':14}
matplotlib.rc('font', **font)



def modelInfo(csvFPath, modelName, simpleAnalysisLocation):
    print("Dumping information on", modelName)
    with open(csvFPath, newline='') as csvfile:
        modelInfoReader = csv.DictReader(csvfile)
        for row in modelInfoReader:
            if row['modelName'] == modelName:
                #print(row.keys())
                print("Found model")
                print(f'Cross section: {round(float(row["Cross_section_EW"])*1000, 1)} fb')
                analysisYields = {}
                masses = {}
                bfs = {}
                
                for key in row:
                    if 'EWSmear_ExpectedEvents_' in key:
                        val = float(row[key])
                        if val > 0:
                            analysisYields[key.split('EWSmear_ExpectedEvents_')[1]] = val
                    if 'm_' in key[:2]:
                        try:
                            val = float(row[key])
                        except:
                            print("Couldn't convert the following to float:", key, row[key])
                            continue
                        if val > 0 and val < 2000:
                            masses[key.split('m_')[1]] = val
                    if 'BF_' in key:
                        val = float(row[key])
                        if val > 0:
                            bfs[key.split('BF_')[1]] = val


                sortedYields = {k: v for k, v in sorted(analysisYields.items(), key=lambda item: item[1], reverse=True)[:5]}
                sortedMasses = {k: v for k, v in sorted(masses.items(), key=lambda item: item[1], reverse=True)}
                sortedBFs = {k: v for k, v in sorted(bfs.items(), key=lambda item: item[1], reverse=True) if k[:2] != 'e_' and k[:3] != 'mu_' and k[:4] != 'tau_' and k[:3] != 'nu_' and v > 0.1}
                print("Model is excluded:", row['Excluded'])
                print("Analyses with the highest yields:")
                for analysis in sortedYields:
                    if 'SR' not in analysis:
                        continue
                    aCode = subprocess.check_output(f'grep Define.*{analysis.split("__")[0]} {simpleAnalysisLocation}/*', shell=True).decode('utf-8').rstrip().split(":")[0].split('/')[-1].rstrip('.cxx')
                    print(f'{aCode:16}, {analysis:40}: {round(sortedYields[analysis],1):5}')
                print("\nMass spectrum:")
                for particle in sortedMasses:
                    print(f'{particle:7}: {int(round(sortedMasses[particle],0)):4} GeV')
                print("\nBranching Fractions:")
                for decay in sortedBFs:
                    print(f'{decay:20}: {int(round(sortedBFs[decay]*100,0)):3}%')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-p', '--path', default='ewk_models_for_walter.csv')
    parser.add_argument('-m', '--modelName', default='8494.0')
    parser.add_argument('-s', '--simpleAnalysisLocation', default='~/SimpleAnalysis/SimpleAnalysisCodes/src/')
    
    args = parser.parse_args()

    modelInfo(args.path, args.modelName, args.simpleAnalysisLocation)

